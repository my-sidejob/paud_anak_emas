<?php 

return [
    'Friday' => "Jum'at",
    'Thursday' => 'Kamis',
    'Wednesday' => 'Rabu',
    'Tuesday' => 'Selasa',
    'Monday' => 'Senin',
    'Sunday' => 'Minggu',
];