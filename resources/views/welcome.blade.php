@extends('layouts.app')

@section('title', 'Welcome')

@section('content')

<body class="custom-bg-gradient">

    <div class="container">
  
        <!-- Outer Row -->
        <div class="row justify-content-center">
            
            <div class=" col-md-6">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="p-4">
                                    <div class="text-center">
                                        <img src="{{ asset('img/logo.jpeg') }}" alt="logo" width="170" class="mb-2">
                                        <h1 class="h4 text-gray-900 mb-4">Selamat Datang!</h1>
                                        <hr>
                                        <p>Orangtua siswa silahkan login untuk melihat aktivitas anak anda pada PAUD Anak Emas.</p>
                                        @if(session()->has('error'))
                                        <hr>
                                        <p class="text-danger">{{ session()->get('error') }}</p>
                                        @endif
                                    </div>
                                    <form class="user" action="{{ route('orangtua.login') }}" method="post">
                                        @csrf
                                        <div class="form-group">
                                            <input type="text" name="username" class="form-control form-control-user" id="exampleInputusername" aria-describedby="usernameHelp" placeholder="Enter username...">
                                            @error('username')
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
                                            @error('password')
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" name="remember" id="customCheck" {{ old('remember') ? 'checked' : '' }}>
                                                <label class="custom-control-label" for="customCheck">Remember Me</label>
                                            </div>               
                                        </div>
                                        
                                        <button type="submit" class="btn btn-warning btn-user btn-block">
                                            Login
                                        </button>                    
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="{{ url('register') }}">Daftarkan Anak Anda Disini!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

@endsection