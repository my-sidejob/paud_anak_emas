@extends('layouts.app')

@section('title', 'Form Pembayaran')

@section('content')

<div class="container-fluid">

  <div class="row">
      <div class="col-md-12 mb-4">
          <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    Form Pembayaran PAUD Anak Emas
                </div>
                <div class="card-body">
                    Silahkan lengkapi form dibawah, pilih jenis pembayaran <small>(<a href="{{ route('ortu.jenis-pembayaran') }}" target="_blank">klik disini untuk melihat jenis pembayaran yang tersedia</a>)</small> dan upload bukti transfer.
                    <hr>
                    <form action="{{ route('ortu.pembayaran.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row custom-fields">
                            <div class="col-sm-6 mb-2">
                                <select name="siswa_id" class="form-control mySelect" style="text-align: left">
                                    <option value=""> - Pilih Anak Anda -</option>
                                    @foreach($siswa as $option)
                                        <option value="{{ $option->id }}" {{ (old('siswa_id') == $option->id) ? 'selected' : '' }}>{{ ucwords($option->nama_lengkap) }}</option>
                                    @endforeach
                                </select>
                                @error('siswa_id')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                           
                            </div>
                        </div>     
                        <div class="form-group row custom-fields">
                            <div class="col-sm-6 mb-2">
                                <label for="">Jenis Pembayaran</label>
                                <select name="jenis_pembayaran_id" class="form-control mySelect" style="text-align: left">
                                    <option value=""> - Pilih Jenis Pembayaran -</option>
                                    @foreach($jenis_pembayaran as $option)
                                        <option value="{{ $option->id }}" {{ (old('jenis_pembayaran_id') == $option->id) ? 'selected' : '' }}>{{ ucwords($option->nama_pembayaran) . ' - Rp. ' . number_format($option->biaya) }}</option>
                                    @endforeach
                                </select>
                                @error('jenis_pembayaran_id')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                           
                            </div>
                        </div> 
                        
                        <div class="form-group row">
                            <div class="col-sm-6 mb-2">
                                <label>Bukti Transfer</label>
                                <input type="file" class="form-control form-control-user" name="bukti_transfer" value="{{ old('bukti_transfer') }}">
                                @error('bukti_transfer')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
    
                        <div class="form-group row">
                            <div class="col-sm-6 mb-2">
                                <label>Catatan (opsional)</label>
                                <textarea class="form-control form-control-user" name="keterangan">{{ old('keterangan') }}</textarea>
                                @error('keterangan')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6 mb-2 ">
                                <input type="submit" class="btn btn-warning float-right" value="Simpan">
                            </div>
                        </div>

                    </form>

                </div>
          </div>
      </div>
  </div>
</div>
@endsection
