@extends('layouts.app')

@section('title', 'Detail Pembelajaran')

@section('content')

<div class="container-fluid">
    <div class="row mb-4">
        <div class="col-md-12">
            <a href="{{ route('ortu.pembelajaran') .'?siswa_id=' . $siswa->id   }}"><i class="fa fa-arrow-left"></i> Kembali </a>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-md-12">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Informasi</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Jadwal</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Nilai</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <!-- INFORMASI -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header font-weight-bold text-primary">
                                            Detail Kelas
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table">
                                                        <tr>
                                                            <th style="border-top:0">Nama Kelas</th>
                                                            <th style="border-top:0">:</th>
                                                            <td style="border-top:0">{{ ucwords($kelas->nama_kelas) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Tipe Kelas</th>
                                                            <th>:</th>
                                                            <td>{{ ucwords($kelas->tipe_kelas) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Tahun Ajar</th>
                                                            <th>:</th>
                                                            <td>{{ $kelas->tahun_ajar }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Status Kelas</th>
                                                            <th>:</th>
                                                            <td>{!! $kelas->getStatusBadge() !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Keterangan</th>
                                                            <th>:</th>
                                                            <td>{{ $kelas->keterangan }}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header font-weight-bold text-primary">
                                            Identitas Siswa
                                        </div>
                                        <div class="card-body">
                                            <table class="table">
                                                <tr>
                                                    <th style="border-top:0">Nama Siswa</th>
                                                    <th style="border-top:0">:</th>
                                                    <td style="border-top:0">{{ ucwords($siswa->nama_lengkap) }}</td>
                                                </tr>
                                                <tr>
                                                    <th>TTL</th>
                                                    <th>:</th>
                                                    <td>{{ ucwords($siswa->tempat_lahir) . ', ' . $siswa->tanggal_lahir }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Umur</th>
                                                    <th>:</th>
                                                    <td>{{ $siswa->getAge() }} Tahun</td>
                                                </tr>
                                                <tr>
                                                    <th>Jenis Kelamin</th>
                                                    <th>:</th>
                                                    <td>{!! $siswa->getJenisKelamin() !!}</td>
                                                </tr>
                                                <tr>
                                                    <th>Status Siswa</th>
                                                    <th>:</th>
                                                    <td>{!! $siswa->getStatusBadge() !!}</td>
                                                </tr>
                                                <tr>
                                                    <th>Orangtua</th>
                                                    <th>:</th>
                                                    <td>{{ ucwords($siswa->orangtua->nama_lengkap) }}</td>
                                                </tr>
                                            </table>
                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                        
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <!-- JADWAL -->
                    <div class="card">
                        
                        <div class="card-body">
                            <h5>Jadwal Kelas {{ ucwords($kelas->nama_kelas). ' ' .ucwords($kelas->tipe_kelas) . ' ' . $kelas->tahun_ajar }}</h5>
                            
                            @foreach($jadwal as $week)
                                <hr style="margin-bottom: 2.3rem!important;">
                                <div class="row justify-content-center">                        
                                    @foreach($week->getJadwalOfWeek() as $row)
                                    <div class="col-md-6 mb-4">
                                        <div class="card">
                                            <div class="card-header font-weight-bold text-primary">                                        
                                                {{ $row->getFormatTanggal() }}
                                            </div>
                                            <div class="card-body">
                                                <table class="table table-sm table-jadwal">
                                                    <tr>
                                                        <th style="border-top:0">Nama</th>
                                                        <th style="border-top:0">Sentra</th>
                                                        <th style="border-top:0; min-width: 105px;">Jam</th>
                                                    </tr>
                                                    @foreach($row->getJadwalOfDate() as $jad)
                                                    <tr>
                                                        <td>{{ $jad->kegiatan->nama_kegiatan }}</td>
                                                        <td>{{ $jad->kegiatan->sentra->nama_sentra }}</td>
                                                        <td>{{ $jad->jam_mulai .' - '. $jad->jam_selesai }}</td>
                                                    </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach                                    
                                </div>
                                
                            @endforeach
                            <br>
                            
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <!-- NILAI -->
                    <div class="card">
                        
                        <div class="card-body">
                            <h5>Tabel Nilai</h5>
                            <hr>
                            <table class="table table-sm" id="myTable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Kegiatan / Pelajaran</th>
                                        <th>Penilaian</th>
                                        <th>Guru</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($nilai as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->kegiatan->nama_kegiatan }}</td>
                                        <td>{{ $row->catatan_penilaian }}</td>
                                        <td>{{ ucwords($row->user->nama_lengkap) }}</td>
                                       
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection


@push('scripts')

<script>
    $('.sidebar-pembayaran').removeClass('active');
</script>

@endpush
