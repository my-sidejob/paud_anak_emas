@extends('layouts.app')

@section('title', 'Form Kritik & Saran')

@section('content')

<div class="container-fluid">

  <div class="row">
      <div class="col-md-6 mb-4">
          <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    Form Kritik dan Saran PAUD Anak Emas
                </div>
                <div class="card-body">
                    Masukan kritik dan saran anda terhadap kami melalui form dibawah.
                    <hr>
                    <form action="{{ route('ortu.kritik-saran.store') }}" method="post">
                        @csrf                                                
                        
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Kritk & Saran</label>
                                <textarea class="form-control form-control-user" name="isi">{{ old('isi') }}</textarea>
                                @error('isi')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12 mb-2 ">
                                <input type="submit" class="btn btn-warning float-right" value="Simpan">
                            </div>
                        </div>

                    </form>

                </div>
          </div>
      </div>
  </div>
</div>
@endsection
