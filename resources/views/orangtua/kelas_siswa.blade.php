@extends('layouts.app')

@section('title', 'Kelas Siswa')

@section('content')

<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    {{ $siswa->nama_lengkap }}
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Kelas</th>
                                <th>Status Kelas</th>
                                <th>Status Siswa</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($siswa->kelasSiswa as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ ucwords($row->nama_kelas) . ' ' . ucwords($row->tipe_kelas) . ' ' . $row->tahun_ajar }}</td>
                                <td>{!! $row->getStatusBadge() !!}</td>
                                <td>{!! $siswa->getStatusBadge() !!}</td>
                                <td><a href="{{ route('ortu.pembelajaran.show', [$siswa->id, $row->id]) }}" class="btn btn-sm btn-info">Detail</a></td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')

<script>
    $('.sidebar-pembayaran').removeClass('active');
</script>

@endpush
