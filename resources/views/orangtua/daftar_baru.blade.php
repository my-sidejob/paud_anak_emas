@extends('layouts.app')

@section('title', 'Daftar Anak Lain')

@section('content')

<div class="container-fluid">

  <div class="row">
      <div class="col-md-6 mb-4">
          <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    Form Untuk Mendaftarkan Anak Anda Yang Lain
                </div>
                <div class="card-body">
                    Untuk melakukan pendaftaran, silahkan isi seluruh form dibawah.
                    <hr>
                    <form action="{{ route('ortu.daftar-baru.store') }}" method="post">
                        @csrf                                                
                        
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Nama Lengkap Anak</label>
                                <input type="text" class="form-control form-control-user" name="nama_lengkap" value="{{ old('nama_lengkap') }}">
                                @error('nama_lengkap')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>                        

                        <div class="form-group row">
                            <div class="col-sm-6 mb-2">
                                <label>Tempat Lahir Anak</label>
                                <input type="text" class="form-control form-control-user" name="tempat_lahir" value="{{ old('tempat_lahir') }}">
                                @error('tempat_lahir')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-sm-6 mb-2">
                                <label>Tanggal Lahir Anak</label>
                                <input type="text" class="form-control form-control-user datepicker" name="tanggal_lahir" value="{{ old('tanggal_lahir') }}">
                                @error('tanggal_lahir')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                       
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">                            
                                <label>Jenis Kelamin Anak</label>
                                <select name="jenis_kelamin" class="form-control">
                                    <option value=""> - Pilih Jenis Kelamin -</option>
                                    <option value="l" {{ (old('jenis_kelamin') == 'l') ? 'selected' : '' }}>Laki - Laki</option>
                                    <option value="p" {{ (old('jenis_kelamin') == 'p') ? 'selected' : '' }}>Perempuan</option>                                    
                                </select>
                                @error('jenis_kelamin')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                           
                            </div>
                        </div>
                        <div class="form-group row">                                    
                            <div class="col-sm-12 mb-2">
                                <label>Tahun Ajaran</label>
                                <input type="text" class="form-control form-control-user" name="tahun_ajar" value="{{ old('tahun_ajar') }}" placeholder="contoh: 2019/2020">
                                @error('tahun_ajar')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">                            
                                <label>Kelompok</label>
                                <select name="jenis_kelompok" class="form-control">
                                    <option value=""> - Pilih Kelompok -</option>
                                    <option value="a" {{ (old('jenis_kelompok') == 'a') ? 'selected' : '' }}>Kelompok A (Usia 4 - 5 thn)</option>
                                    <option value="b" {{ (old('jenis_kelompok') == 'b') ? 'selected' : '' }}>Kelompok B (Usia 5 - 6 thn)</option>
                                </select>
                                @error('jenis_kelompok')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                           
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12 mb-2 ">
                                <input type="submit" class="btn btn-warning float-right" value="Simpan">
                            </div>
                        </div>

                    </form>

                </div>
          </div>
      </div>
  </div>
</div>
@endsection
