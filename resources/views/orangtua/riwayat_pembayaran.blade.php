@extends('layouts.app')

@section('title', 'Riwayat Pembayaran')

@section('content')

<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    Riwayat Pembayaran Anda
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tanggal</th>
                                <th>Jenis Pembayaran</th>
                                <th>Siswa</th>
                                <th>Bukti Transfer</th>
                                <th>Keterangan</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pembayaran as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->tanggal_pembayaran }}</td>
                                <td>{{ ucwords($row->jenisPembayaran->nama_pembayaran) }}</td>
                                <td>{{ ucwords($row->siswa->nama_lengkap) }}</td>
                                <td> <a href="{{ url('storage/bukti_transfer/'.$row->bukti_pembayaran) }}" target="_blank">Klik Disini</a> </td>
                                <td>{{ $row->keterangan }}</td>
                                <td>{!! $row->getStatusBadge() !!}</td>
                                
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')

<script>
    $('.sidebar-pembayaran').removeClass('active');
</script>

@endpush
