@extends('layouts.app')

@section('title', 'Pembelajaran')

@section('content')

<div class="container-fluid">

    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    Pilih Siswa
                </div>
                <div class="card-body">
                    <form action="{{ route('ortu.pembelajaran') }}" method="get">
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <select name="siswa_id" class="form-control mySelect">
                                    <option value=""> - Pilih -</option>
                                    @foreach($siswa as $option)
                                        <option value="{{ $option->id }}" {{ (old('siswa_id') == $option->id) ? 'selected' : '' }}>{{ ucwords($option->nama_lengkap) }} </option>
                                    @endforeach
                                </select>
                                @error('siswa_id')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> 
                        <button type="submit" class="btn btn-warning mb-4 btn-sm"><i class="fa fa-search"></i> Cari</button>
                    </form>

                    
                </div>
            </div>
        </div>
    </div>
</div>


@endsection