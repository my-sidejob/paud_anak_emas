<ul class="navbar-nav custom-bg-gradient sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('dashboard')}}">
        <div class="sidebar-brand-icon">
            <i class="fa fa-child"></i>
          </div>
        <div class="sidebar-brand-text mx-3">PAUD Anak Emas</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">
    @if(Auth::guard('web')->check())
        <!-- Nav Item - Dashboard -->
        <li class="nav-item {{ (urlHasPrefix('dashboard') == true ) ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('dashboard') }}">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span>
            </a>
        </li>
        <hr class="sidebar-divider">
        <div class="sidebar-heading">
            Master
        </div>
        <li class="nav-item {{ (urlHasPrefix('sentra') == true ) ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('sentra.index') }}">
                <i class="fas fa-comments"></i>
                <span>Sentra</span>
            </a>        
        </li>

        <li class="nav-item {{ (urlHasPrefix('kelas') == true ) ? 'active' : '' }} sidebar-link">
            <a class="nav-link" href="{{ route('kelas.index') }}">
                <i class="fas fa-school"></i>
                <span>Kelas</span>
            </a>        
        </li>

        <li class="nav-item {{ (urlHasPrefix('kegiatan') == true ) ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('kegiatan.index') }}">
                <i class="fas fa-list-alt"></i>
                <span>Kegiatan / Pelajaran</span>
            </a>        
        </li>

        <li class="nav-item {{ (urlHasPrefix('orangtua') == true ) ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('orangtua.index') }}">
                <i class="fas fa-user-tie"></i>
                <span>Orangtua</span>
            </a>
        </li>

        <li class="nav-item {{ (urlHasPrefix('siswa') == true ) ? 'active' : '' }} sidebar-link">
            <a class="nav-link" href="{{ route('siswa.index') }}">
                <i class="fas fa-child"></i>
                <span>Siswa</span>
            </a>
        </li>
        @if(Auth::guard('web')->user()->level == 2)
        <li class="nav-item {{ (urlHasPrefix('user') == true ) ? 'active' : '' }} sidebar-link">
            <a class="nav-link" href="{{ route('user.index') }}">
                <i class="fas fa-user"></i>
                <span>Admin / Guru</span>
            </a>
        </li>
        @endif
        
        <li class="nav-item {{ (urlHasPrefix('jenis-pembayaran') == true ) ? 'active' : '' }} sidebar-link">
            <a class="nav-link" href="{{ route('jenis-pembayaran.index') }}">
                <i class="fas fa-dollar-sign"></i>
                <span>Jenis Pembayaran</span>
            </a>
        </li>

        <hr class="sidebar-divider">
        <div class="sidebar-heading">
            Aktifitas
        </div>
        <li class="nav-item {{ (urlHasPrefix('kelas-siswa') == true ) ? 'active' : '' }} sidebar-pembelajaran">
            <a class="nav-link" href="{{ route('kelas-siswa.index') }}">
                <i class="fas fa-swatchbook"></i>
                <span>Pembelajaran</span>
            </a>
        </li>

        <li class="nav-item {{ (urlHasPrefix('pembayaran') == true ) ? 'active' : '' }} sidebar-whatever">
            <a class="nav-link" href="{{ route('pembayaran.index') }}">
                <i class="fas fa-money-check"></i>
                <span>Pembayaran</span>
            </a>
        </li>
        @if(Auth::guard('web')->user()->level == 2)
        <hr class="sidebar-divider">
        <div class="sidebar-heading">
            Extra
        </div>
        <li class="nav-item {{ (urlHasPrefix('kritik-saran') == true ) ? 'active' : '' }} sidebar-link">
            <a class="nav-link" href="{{ route('kritik-saran.index') }}">
                <i class="fas fa-clipboard"></i>
                <span>Kritik & Saran</span>
            </a>
        </li>
        @endif

    @endif

    @if(Auth::guard('orangtua')->check())
    <li class="nav-item {{ (urlHasPrefix('dashboard') == true ) ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('ortu/dashboard') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Aktifitas
    </div>
    <li class="nav-item {{ (urlHasPrefix('pembelajaran') == true ) ? 'active' : '' }} sidebar-pembelajaran">
        <a class="nav-link" href="{{ route('ortu.pembelajaran') }}">
            <i class="fas fa-swatchbook"></i>
            <span>Pembelajaran</span>
        </a>
    </li>

    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Menu Pembayaran
    </div>
    <li class="nav-item {{ (urlHasPrefix('jenis-pembayaran') == true ) ? 'active' : '' }} sidebar-link">
        <a class="nav-link" href="{{ route('ortu.jenis-pembayaran') }}">
            <i class="fas fa-dollar-sign"></i>
            <span>Jenis Pembayaran</span>
        </a>
    </li>
    <li class="nav-item {{ (urlHasPrefix('pembayaran') == true ) ? 'active' : '' }} sidebar-pembayaran">
        <a class="nav-link" href="{{ route('ortu.pembayaran') }}">
            <i class="fas fa-money-check-alt"></i>
            <span>Pembayaran</span>
        </a>
    </li>

    <li class="nav-item {{ (urlHasPrefix('riwayat-pembayaran') == true ) ? 'active' : '' }} sidebar-riwayat">
        <a class="nav-link" href="{{ route('ortu.riwayat-pembayaran') }}">
            <i class="fas fa-history"></i>
            <span>Riwayat Pembayaran</span>
        </a>
    </li>

    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Extra
    </div>
    <li class="nav-item {{ (urlHasPrefix('kritik-saran') == true ) ? 'active' : '' }} sidebar-link">
        <a class="nav-link" href="{{ route('ortu.kritik-saran') }}">
            <i class="fas fa-clipboard"></i>
            <span>Kritik & Saran</span>
        </a>
    </li>
    <li class="nav-item {{ (urlHasPrefix('daftar-baru') == true ) ? 'active' : '' }} sidebar-link">
        <a class="nav-link" href="{{ route('ortu.daftar-baru') }}">
            <i class="fas fa-user-plus"></i>
            <span>Daftar Anak Lain</span>
        </a>
    </li>
    @endif
    

    
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>