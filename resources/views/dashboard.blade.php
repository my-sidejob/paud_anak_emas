@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')

<div class="container-fluid">

  <div class="row">
      <div class="col-md-12">
          <div class="card">
              <div class="card-header font-weight-bold text-primary">
                  Selamat Datang
              </div>
              <div class="card-body">
                  Sistem Informasi Manajemen PAUD Anak Emas Berbasis Website
              </div>
          </div>
      </div>
  </div>
</div>
@endsection
