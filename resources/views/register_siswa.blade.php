@extends('layouts.app')

@section('title', 'Welcome')

@section('content')

<body class="custom-bg-gradient">

    <div class="container">
  
        <!-- Outer Row -->
        <div class="row justify-content-center">
            
            <div class=" col-md-8">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="p-4">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Form Register Siswa</h1>
                                        <hr>
                                        <p>Untuk melakukan pendaftaran, silahkan isi seluruh form dibawah.</p>
                                        <hr>
                                        @if(session()->has('error'))
                                        <hr>
                                        <p class="text-danger">{{ session()->get('error') }}</p>
                                        @endif
                                    </div>
                                    <form class="user" action="{{ route('register.store') }}" method="post">
                                        @csrf
                                        <div class="form-group row">
                                            <div class="col-sm-12 mb-2">
                                                <label>Nama Lengkap Anak</label>
                                                <input type="text" class="form-control form-control-user" name="nama_lengkap_anak" value="{{ old('nama_lengkap_anak', $field->nama_lengkap_anak) }}">
                                                @error('nama_lengkap_anak')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>                        
                
                                        <div class="form-group row">
                                            <div class="col-sm-6 mb-2">
                                                <label>Tempat Lahir Anak</label>
                                                <input type="text" class="form-control form-control-user" name="tempat_lahir" value="{{ old('tempat_lahir', $field->tempat_lahir) }}">
                                                @error('tempat_lahir')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-sm-6 mb-2">
                                                <label>Tanggal Lahir Anak</label>
                                                <input type="text" class="form-control form-control-user datepicker" name="tanggal_lahir" value="{{ old('tanggal_lahir', $field->tanggal_lahir) }}">
                                                @error('tanggal_lahir')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                       
                                        <div class="form-group row">
                                            <div class="col-sm-12 mb-2">                            
                                                <label>Jenis Kelamin Anak</label>
                                                <select name="jenis_kelamin" class="form-control">
                                                    <option value=""> - Pilih Jenis Kelamin -</option>
                                                    <option value="l" {{ (old('jenis_kelamin') == 'l' || $field->jenis_kelamin == 'l') ? 'selected' : '' }}>Laki - Laki</option>
                                                    <option value="p" {{ (old('jenis_kelamin') == 'p' || $field->jenis_kelamin == 'p') ? 'selected' : '' }}>Perempuan</option>                                    
                                                </select>
                                                @error('jenis_kelamin')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror                           
                                            </div>
                                        </div>
                                        <div class="form-group row">                                    
                                            <div class="col-sm-12 mb-2">
                                                <label>Tahun Ajaran</label>
                                                <input type="text" class="form-control form-control-user" name="tahun_ajar" value="{{ old('tahun_ajar') }}" placeholder="contoh: 2019/2020">
                                                @error('tahun_ajar')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12 mb-2">                            
                                                <label>Kelompok</label>
                                                <select name="jenis_kelompok" class="form-control">
                                                    <option value=""> - Pilih Kelompok -</option>
                                                    <option value="a" {{ (old('jenis_kelompok') == 'a') ? 'selected' : '' }}>Kelompok A (Usia 4 - 5 thn)</option>
                                                    <option value="b" {{ (old('jenis_kelompok') == 'b') ? 'selected' : '' }}>Kelompok B (Usia 5 - 6 thn)</option>
                                                </select>
                                                @error('jenis_kelompok')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror                           
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group row">
                                            <div class="col-sm-12 mb-2">
                                                <label>Nama Orangtua</label>
                                                <input type="text" class="form-control form-control-user" name="nama_lengkap" value="{{ old('nama_lengkap', $orangtua->nama_lengkap) }}">
                                                @error('nama_lengkap')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>                        
                
                                        <div class="form-group row">
                                            <div class="col-sm-12 mb-2">
                                                <label>Username</label>
                                                <input type="text" class="form-control form-control-user" name="username" value="{{ old('username', $orangtua->username) }}">
                                                @error('username')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        @if(!isset($orangtua->id))
                                        <div class="form-group row">
                                            <div class="col-sm-12 mb-2">
                                                <label>Password</label>
                                                <input type="password" class="form-control form-control-user" name="password" value="{{ old('password', $orangtua->password) }}">
                                                @error('password')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        @endif
                                        
                                        <div class="form-group row">
                                            <div class="col-sm-12 mb-2">
                                                <label>Email</label>
                                                <input type="email" class="form-control form-control-user" name="email" value="{{ old('email', $orangtua->email) }}">
                                                @error('email')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                
                                        <div class="form-group row">
                                            <div class="col-sm-12 mb-2">
                                                <label>No. Telp</label>
                                                <input type="number" class="form-control form-control-user" name="no_telp" value="{{ old('no_telp', $orangtua->no_telp) }}">
                                                @error('no_telp')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                 
                
                                        <div class="form-group row">
                                            <div class="col-sm-12 mb-2">
                                                <label>Alamat</label>
                                                <textarea class="form-control form-control-user" name="alamat">{{ old('alamat', $orangtua->alamat) }}</textarea>
                                                @error('alamat')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        
                                        <button type="submit" class="btn btn-warning btn-user btn-block">
                                            Daftar
                                        </button>                    
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="{{ url('/') }}">Kembali ke halaman login</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
        $( ".datepicker" ).datepicker({ 
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,
            yearRange: "1950:2020"
        });
        } );
    </script>

</body>

@endsection

