<div class="modal-header">
    <h5 class="modal-title">Tambah Siswa Ke Kelas</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form action="{{ route('kelas-siswa.store') }}" method="post">
    @csrf
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group row custom-fields">
                    <div class="col-sm-12 mb-2">
                        <input type="hidden" name="kelas_id" value="{{ $kelas_id }}">
                        <select name="siswa_id" class="form-control mySelect" style="text-align: left">
                            <option value=""> - Pilih Siswa -</option>
                            @foreach($siswa as $option)
                                <option value="{{ $option->id }}" {{ (old('siswa_id') == $option->id) ? 'selected' : '' }}>{{ ucwords($option->nama_lengkap) }}</option>
                            @endforeach
                        </select>
                        @error('siswa_id')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror                           
                    </div>
                </div>         
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-warning">Simpan</button>
    </div>
</form>


<script>
    $(document).ready( function () {
        //select2 input option
        $('.mySelect').select2();
    } );
  </script>