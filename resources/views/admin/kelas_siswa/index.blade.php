@extends('layouts.app')

@section('title', 'Kelas Siswa')

@section('content')

<div class="container-fluid">

    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    Pilih Kelas
                </div>
                <div class="card-body">
                    <form action="{{ route('kelas-siswa.index') }}" method="get">
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <select name="kelas_id" class="form-control mySelect">
                                    <option value=""> - Pilih kelas -</option>
                                    @foreach($kelas as $option)
                                        <option value="{{ $option->id }}" {{ (old('kelas_id') == $option->id) ? 'selected' : '' }}>{{ ucwords($option->nama_kelas) }} {{ ucwords($option->tipe_kelas) }} {{ $option->tahun_ajar }} </option>
                                    @endforeach
                                </select>
                                @error('kelas_id')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> 
                        <button type="submit" class="btn btn-warning mb-4 btn-sm"><i class="fa fa-search"></i> Cari</button>
                    </form>

                    
                </div>
            </div>
        </div>
    </div>
</div>


@endsection


@push('scripts')

<script>
    $('.sidebar-link').removeClass('active');
</script>

@endpush