@extends('layouts.app')

@section('title', 'Detail Kelas dan Daftar Siswa')

@section('content')

<div class="container-fluid">
    <div class="row mb-4">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    Detail Kelas
                </div>
                <div class="card-body">
                    <table class="table">
                        <tr>
                            <th style="border-top:0">Nama Kelas</th>
                            <th style="border-top:0">:</th>
                            <td style="border-top:0">{{ ucwords($kelas->nama_kelas) }}</td>
                        </tr>
                        <tr>
                            <th>Tipe Kelas</th>
                            <th>:</th>
                            <td>{{ ucwords($kelas->tipe_kelas) }}</td>
                        </tr>
                        <tr>
                            <th>Tahun Ajar</th>
                            <th>:</th>
                            <td>{{ $kelas->tahun_ajar }}</td>
                        </tr>
                        <tr>
                            <th>Status </th>
                            <th>:</th>
                            <td>{!! $kelas->getStatusBadge() !!}</td>
                        </tr>
                        <tr>
                            <th>Keterangan</th>
                            <th>:</th>
                            <td>{{ $kelas->keterangan }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    Jadwal Minggu Terbaru
                </div>
                <div class="card-body">
                    <div class="div">
                        <div class="d-inline-block">
                            <a href="{{ route('jadwal.create') .'?kelas_id=' . $kelas->id }}" class="btn btn-secondary btn-sm btn-jadwal mb-3">Tambah Jadwal</a>
                        </div>
                        <div class="d-inline-block align-top">
                            <a href="{{ route('jadwal.kelas', $kelas->id) }}" class="btn btn-sm btn-primary">Lihat Semua Jadwal</a>
                        </div>
                    </div>
                    <table class="table">
                        <tr>
                            <td>#</td>
                            <td>Hari / Tanggal</td>
                            <td>Detail</td>
                        </tr>
                        @forelse($jadwal as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->getFormatTanggal() }}</td>
                            <td><a href="{{ route('jadwal.detail_hari', [$kelas->id, $row->tanggal]) }}" title="Lihat Jadwal Pada Hari Ini" class="btn btn-sm btn-info btn-jadwal-harian"><i class="fa fa-eye"></i></a></td>
                        </tr>
                        @empty 
                        <tr>
                            <td colspan="3">Belum ada jadwal</td>
                        </tr>
                        @endforelse
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    Daftar Siswa Pada Kelas Ini
                </div>
                <div class="card-body">

                    <a href="{{ route('kelas-siswa.create') .'?kelas_id=' . $kelas->id }}" class="btn btn-warning mb-4 btn-sm btn-show"><i class="fa fa-plus"></i> Tambah Siswa</a>

                    <table class="table table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Siswa</th>                            
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($kelas->kelasSiswa as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ ucwords($row->nama_lengkap) }}</td>                               
                                <td>
                                    <form action="{{ route('kelas-siswa.destroy', $row->id) }}" method="post">                                                                               
                                        @csrf
                                        <input type="hidden" name="siswa_id" value="{{ $row->id }}">
                                        @method('delete')
                                        <a href="{{ route('nilai.index', [$row->id, $kelas->id]) }}" class="btn btn-sm btn-success">Nilai</a>
                                        <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Yakin hapus siswa dari kelas?')" >
                                            <span class="icon text-white">
                                            <i class="fas fa-trash"></i>
                                            </span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="theModal" class="modal fade text-center">
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>

<div id="theModal-lg" class="modal fade text-center">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
        </div>
    </div>
</div>

<div id="theModal-lg-jadwal" class="modal fade text-center">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
        </div>
    </div>
</div>


@endsection


@push('scripts')

<script>
    $('.btn-show').on('click', function(e){
        e.preventDefault();
        $('#theModal').modal('show').find('.modal-content').load($(this).attr('href'));
    });

    $('.btn-jadwal').on('click', function(e){
        e.preventDefault();
        $('#theModal-lg').modal('show').find('.modal-content').load($(this).attr('href'));
    });

    $('.btn-jadwal-harian').on('click', function(e){
        e.preventDefault();
        $('#theModal-lg-jadwal').modal('show').find('.modal-content').load($(this).attr('href'));
    });
</script>

<script>
    $('.sidebar-link').removeClass('active');
</script>
@endpush