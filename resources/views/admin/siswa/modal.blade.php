<div class="modal-header">
    <h5 class="modal-title">Detail Siswa</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <table class="table text-left">
                <tr>
                    <th style="border-top:0">Nama Siswa</th>
                    <td style="border-top:0">:</td>
                    <td style="border-top:0">{{ $siswa->nama_lengkap }}</td>
                </tr>
                <tr>
                    <th>Tempat, Tanggal Lahir</th>
                    <td>:</td>
                    <td>{{ $siswa->tempat_lahir . ', ' . $siswa->tanggal_lahir }}</td>
                </tr>
                <tr>
                    <th>Umur</th>
                    <td>:</td>
                    <td>{!! $siswa->getAge() !!} Thn</td>
                </tr>
                <tr>
                    <th>Jenis Kelamin</th>
                    <td>:</td>
                    <td>{{ $siswa->getJenisKelamin() }}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>:</td>
                    <td>{!! $siswa->getStatusBadge() !!}</td>
                </tr>
                <tr>
                    <th>Keterangan</th>
                    <td>:</td>
                    <td>{{ $siswa->keterangan }}</td>
                </tr>
                <tr>
                    <th>Orangtua</th>
                    <td>:</td>
                    <td>{{ $siswa->orangtua->nama_lengkap }}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>