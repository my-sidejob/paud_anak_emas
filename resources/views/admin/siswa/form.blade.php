@extends('layouts.app')

@section('title', 'Form Siswa')

@section('content')

<div class="container-fluid">
  
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-8 mb-4">
            <div class="card">
                <div class="card-header">
                    Form Siswa
                </div>
                <div class="card-body">
                    <form action="{{ (!isset($siswa->id)) ?  route('siswa.store') : route('siswa.update', $siswa->id) }}" method="post">
                        @csrf
        
                        @if(isset($siswa->id))
                            @method('put')
                        @endif

                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">                            
                                <label>Orangtua</label>
                                <select name="orangtua_id" class="form-control mySelect">
                                    <option value=""> - Pilih orangtua -</option>
                                    @foreach($orangtua as $option)
                                        <option value="{{ $option->id }}" {{ (old('orangtua_id') == $option->id || $siswa->orangtua_id == $option->id) ? 'selected' : '' }}>{{ ucwords($option->nama_lengkap) }}</option>
                                    @endforeach
                                </select>
                                @error('orangtua_id')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                           
                            </div>
                        </div> 
        
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Nama lengkap</label>
                                <input type="text" class="form-control form-control-user" name="nama_lengkap" value="{{ old('nama_lengkap', $siswa->nama_lengkap) }}">
                                @error('nama_lengkap')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>                        

                        <div class="form-group row">
                            <div class="col-sm-6 mb-2">
                                <label>Tempat Lahir</label>
                                <input type="text" class="form-control form-control-user" name="tempat_lahir" value="{{ old('tempat_lahir', $siswa->tempat_lahir) }}">
                                @error('tempat_lahir')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-sm-6 mb-2">
                                <label>Tanggal Lahir</label>
                                <input type="text" class="form-control form-control-user datepicker" name="tanggal_lahir" value="{{ old('tanggal_lahir', $siswa->tanggal_lahir) }}">
                                @error('tanggal_lahir')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                       
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">                            
                                <label>Jenis Kelamin</label>
                                <select name="jenis_kelamin" class="form-control">
                                    <option value=""> - Pilih Jenis Kelamin -</option>
                                    <option value="l" {{ (old('jenis_kelamin') == 'l' || $siswa->jenis_kelamin == 'l') ? 'selected' : '' }}>Laki - Laki</option>
                                    <option value="p" {{ (old('jenis_kelamin') == 'p' || $siswa->jenis_kelamin == 'p') ? 'selected' : '' }}>Perempuan</option>                                    
                                </select>
                                @error('jenis_kelamin')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                           
                            </div>
                        </div>                                                                
                        
                        <button type="submit" class="btn btn-warning btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-save"></i>
                            </span>
                            <span class="text">Simpan</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection