@extends('layouts.app')

@section('title', 'Pembayaran')

@section('content')

<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    Data Pembayaran PAUD Anak Emas
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tanggal</th>
                                <th>Jenis Pembayaran</th>
                                <th>Siswa</th>
                                <th>Bukti Transfer</th>
                                <th>Keterangan</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pembayaran as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->tanggal_pembayaran }}</td>
                                <td>{{ ucwords($row->jenisPembayaran->nama_pembayaran) }}</td>
                                <td>{{ ucwords($row->siswa->nama_lengkap) }}</td>
                                <td> <a href="{{ url('storage/bukti_transfer/'.$row->bukti_pembayaran) }}" target="_blank">Klik Disini</a> </td>
                                <td>{{ $row->keterangan }}</td>
                                <td>{!! $row->getStatusBadge() !!}</td>
                                <td>
                                    @if($row->status == 0)
                                        <a href="{{ route('pembayaran.change_status', [$row->id, 1]) }}" class="btn btn-sm btn-success" title="Acc Pembayaran"  onclick="return confirm('Yakin menerima pembayaran?')"><i class="fa fa-check"></i></a>
                                        <a href="{{ route('pembayaran.change_status', [$row->id, 2]) }}" class="btn btn-sm btn-danger" title="Tolak Pembayaran"  onclick="return confirm('Yakin menolak pembayaran?')"><i class="fa fa-times"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')

<script>
    $('.sidebar-pembayaran').removeClass('active');
</script>

@endpush
