@extends('layouts.app')

@section('title', 'Form Sentra')

@section('content')

<div class="container-fluid">
  
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-8 mb-4">
            <div class="card">
                <div class="card-header">
                    Form Sentra
                </div>
                <div class="card-body">
                    <form action="{{ (!isset($sentra->id)) ?  route('sentra.store') : route('sentra.update', $sentra->id) }}" method="post">
                        @csrf
        
                        @if(isset($sentra->id))
                            @method('put')
                        @endif
        
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Nama Sentra</label>
                                <input type="text" class="form-control form-control-user" name="nama_sentra" value="{{ old('nama_sentra', $sentra->nama_sentra) }}">
                                @error('nama_sentra')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Keterangan</label>
                                <textarea class="form-control form-control-user" name="keterangan">{{ old('keterangan', $sentra->keterangan) }}</textarea>
                                @error('keterangan')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <button type="submit" class="btn btn-warning btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-save"></i>
                            </span>
                            <span class="text">Simpan</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection