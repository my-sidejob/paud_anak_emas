@extends('layouts.app')

@section('title', 'Form Kegiatan')

@section('content')

<div class="container-fluid">
  
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-8 mb-4">
            <div class="card">
                <div class="card-header">
                    Form Kegiatan
                </div>
                <div class="card-body">
                    <form action="{{ (!isset($kegiatan->id)) ?  route('kegiatan.store') : route('kegiatan.update', $kegiatan->id) }}" method="post">
                        @csrf
        
                        @if(isset($kegiatan->id))
                            @method('put')
                        @endif
        
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Nama kegiatan</label>
                                <input type="text" class="form-control form-control-user" name="nama_kegiatan" value="{{ old('nama_kegiatan', $kegiatan->nama_kegiatan) }}">
                                @error('nama_kegiatan')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">                            
                                <label>Sentra</label>
                                <select name="sentra_id" class="form-control mySelect">
                                    <option value=""> - Pilih Sentra -</option>
                                    @foreach($sentra as $option)
                                        <option value="{{ $option->id }}" {{ (old('sentra_id') == $option->id || $kegiatan->sentra_id == $option->id) ? 'selected' : '' }}>{{ ucwords($option->nama_sentra) }}</option>
                                    @endforeach
                                </select>
                                @error('sentra_id')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                           
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Keterangan</label>
                                <textarea class="form-control form-control-user" name="keterangan">{{ old('keterangan', $kegiatan->keterangan) }}</textarea>
                                @error('keterangan')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <button type="submit" class="btn btn-warning btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-save"></i>
                            </span>
                            <span class="text">Simpan</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection