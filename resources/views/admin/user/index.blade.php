@extends('layouts.app')

@section('title', 'User / Guru')

@section('content')

<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    Data Master Admin / Guru
                </div>
                <div class="card-body">
                    <a href="{{ route('user.create') }}" class="btn btn-warning mb-4 btn-sm"><i class="fa fa-plus"></i> Tambah</a>

                    <table class="table table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama</th>
                                <th>TTL</th>
                                <th>Email</th>
                                <th>J. Kel</th>
                                <th>Level</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($user as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->nama_lengkap }}</td>
                                <td>{{ $row->tempat_lahir . ', ' . $row->tanggal_lahir }}</td>
                                <td>{{ $row->email }}</td>
                                <td>{{ $row->getJenisKelamin() }}</td>
                                <td>{!! $row->getLevelBadge() !!}</td>
                                <td>
                                    <form action="{{ route('user.destroy', $row->id) }}" method="post">

                                        <a href="{{ route('user.edit', $row->id) }}" class="btn btn-sm btn-warning" title="Edit"><i class="fa fa-cog"></i></a>
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Yakin hapus data?')" >
                                            <span class="icon text-white">
                                            <i class="fas fa-trash"></i>
                                            </span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection


@push('scripts')



@endpush