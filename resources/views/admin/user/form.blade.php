@extends('layouts.app')

@section('title', 'Form Admin / Guru')

@section('content')

<div class="container-fluid">
  
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-8 mb-4">
            <div class="card">
                <div class="card-header">
                    Form Admin / Guru
                </div>
                <div class="card-body">
                    <form action="{{ (!isset($user->id)) ?  route('user.store') : route('user.update', $user->id) }}" method="post">
                        @csrf
        
                        @if(isset($user->id))
                            @method('put')
                        @endif
        
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Nama lengkap</label>
                                <input type="text" class="form-control form-control-user" name="nama_lengkap" value="{{ old('nama_lengkap', $user->nama_lengkap) }}">
                                @error('nama_lengkap')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Username</label>
                                <input type="text" class="form-control form-control-user" name="username" value="{{ old('username', $user->username) }}">
                                @error('username')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        @if(!isset($user->id))
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Password</label>
                                <input type="password" class="form-control form-control-user" name="password" value="{{ old('password', $user->password) }}">
                                @error('password')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        @endif
                        
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Email</label>
                                <input type="email" class="form-control form-control-user" name="email" value="{{ old('email', $user->email) }}">
                                @error('email')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6 mb-2">
                                <label>Tempat Lahir</label>
                                <input type="text" class="form-control form-control-user" name="tempat_lahir" value="{{ old('tempat_lahir', $user->tempat_lahir) }}">
                                @error('tempat_lahir')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-sm-6 mb-2">
                                <label>Tanggal Lahir</label>
                                <input type="text" class="form-control form-control-user datepicker" name="tanggal_lahir" value="{{ old('tanggal_lahir', $user->tanggal_lahir) }}">
                                @error('tanggal_lahir')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6 mb-2">
                                <label>Jenis Kelamin</label>
                                <select name="jenis_kelamin" class="form-control">
                                    <option value=""> - Pilih Jenis Kelamin -</option>
                                    <option value="l" {{ (old('jenis_kelamin') == 'l' || $user->jenis_kelamin == 'l') ? 'selected' : '' }}>Laki - Laki</option>
                                    <option value="p" {{ (old('jenis_kelamin') == 'p' || $user->jenis_kelamin == 'p') ? 'selected' : '' }}>Perempuan</option>                                    
                                </select>
                                @error('jenis_kelamin')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-sm-6 mb-2">
                                <label>Level</label>
                                <select name="level" class="form-control">
                                    <option value=""> - Pilih Level -</option>
                                    <option value="1" {{ (old('level') == '1' || $user->level == '1') ? 'selected' : '' }}>Guru</option>
                                    <option value="2" {{ (old('level') == '2' || $user->level == '2') ? 'selected' : '' }}>Admin</option>                                    
                                </select>
                                @error('level')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        
                        <button type="submit" class="btn btn-warning btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-save"></i>
                            </span>
                            <span class="text">Simpan</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection