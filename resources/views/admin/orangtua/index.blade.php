@extends('layouts.app')

@section('title', 'Orangtua')

@section('content')

<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    Data Master Orangtua
                </div>
                <div class="card-body">
                    <a href="{{ route('orangtua.create') }}" class="btn btn-warning mb-4 btn-sm"><i class="fa fa-plus"></i> Tambah</a>

                    <table class="table table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Ortu</th>
                                <th>Email</th>
                                <th>No. Telp</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orangtua as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->nama_lengkap }}</td>
                                <td>{{ $row->email }}</td>
                                <td>{{ $row->no_telp }}</td>
                                <td>
                                    <form action="{{ route('orangtua.destroy', $row->id) }}" method="post">
                                        <a href="{{ route('orangtua.show', $row->id) }}" class="btn btn-sm btn-info btn-show" title="Detail"><i class="fa fa-eye"></i></a>
                                        <a href="{{ route('orangtua.edit', $row->id) }}" class="btn btn-sm btn-warning" title="Edit"><i class="fa fa-cog"></i></a>
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Yakin hapus data?')" >
                                            <span class="icon text-white">
                                            <i class="fas fa-trash"></i>
                                            </span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="theModal" class="modal fade text-center">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
        </div>
    </div>
</div>


@endsection


@push('scripts')

<script>
    $('.btn-show').on('click', function(e){
        e.preventDefault();
        $('#theModal').modal('show').find('.modal-content').load($(this).attr('href'));
    });
</script>

@endpush