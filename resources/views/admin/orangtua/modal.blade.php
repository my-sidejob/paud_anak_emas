<div class="modal-header">
    <h5 class="modal-title">Detail Orangtua</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <table class="table text-left">
                <tr>
                    <th style="border-top:0">Nama</th>
                    <td style="border-top:0">:</td>
                    <td style="border-top:0">{{ $orangtua->nama_lengkap }}</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>:</td>
                    <td>{{ $orangtua->email }}</td>
                </tr>
                <tr>
                    <th>Username</th>
                    <td>:</td>
                    <td>{{ $orangtua->username }}</td>
                </tr>
                <tr>
                    <th>Alamat</th>
                    <td>:</td>
                    <td>{{ $orangtua->alamat }}</td>
                </tr>
                <tr>
                    <th>No. Telp</th>
                    <td>:</td>
                    <td>{{ $orangtua->no_telp }}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>