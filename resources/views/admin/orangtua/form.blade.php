@extends('layouts.app')

@section('title', 'Form Orangtua')

@section('content')

<div class="container-fluid">
  
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-8 mb-4">
            <div class="card">
                <div class="card-header">
                    Form Orangtua
                </div>
                <div class="card-body">
                    <form action="{{ (!isset($orangtua->id)) ?  route('orangtua.store') : route('orangtua.update', $orangtua->id) }}" method="post">
                        @csrf
        
                        @if(isset($orangtua->id))
                            @method('put')
                        @endif
        
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Nama lengkap</label>
                                <input type="text" class="form-control form-control-user" name="nama_lengkap" value="{{ old('nama_lengkap', $orangtua->nama_lengkap) }}">
                                @error('nama_lengkap')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>                        

                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Username</label>
                                <input type="text" class="form-control form-control-user" name="username" value="{{ old('username', $orangtua->username) }}">
                                @error('username')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        @if(!isset($orangtua->id))
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Password</label>
                                <input type="password" class="form-control form-control-user" name="password" value="{{ old('password', $orangtua->password) }}">
                                @error('password')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        @endif
                        
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Email</label>
                                <input type="email" class="form-control form-control-user" name="email" value="{{ old('email', $orangtua->email) }}">
                                @error('email')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>No. Telp</label>
                                <input type="number" class="form-control form-control-user" name="no_telp" value="{{ old('no_telp', $orangtua->no_telp) }}">
                                @error('no_telp')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                 

                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Alamat</label>
                                <textarea class="form-control form-control-user" name="alamat">{{ old('alamat', $orangtua->alamat) }}</textarea>
                                @error('alamat')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <button type="submit" class="btn btn-warning btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-save"></i>
                            </span>
                            <span class="text">Simpan</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection