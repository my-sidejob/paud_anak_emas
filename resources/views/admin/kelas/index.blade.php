@extends('layouts.app')

@section('title', 'Kelas')

@section('content')

<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    Data Master Kelas
                </div>
                <div class="card-body">
                    <a href="{{ route('kelas.create') }}" class="btn btn-warning mb-4 btn-sm"><i class="fa fa-plus"></i> Tambah</a>

                    <table class="table table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Kelas</th>
                                <th>Tipe Kelas</th>
                                <th>Tahun Ajar</th>
                                <th>Status</th>
                                <th>Keterangan</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($kelas as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->nama_kelas }}</td>
                                <td>{{ ucwords($row->tipe_kelas) }}</td>
                                <td>{{ $row->tahun_ajar }}</td>
                                <td>{!! $row->getStatusBadge() !!}</td>
                                <td>{{ $row->keterangan }}</td>
                                <td>
                                    <form action="{{ route('kelas.destroy', $row->id) }}" method="post">                                       
                                        <a href="{{ route('kelas.edit', $row->id) }}" class="btn btn-sm btn-warning" title="Edit"><i class="fa fa-cog"></i></a>
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Yakin hapus data?')" >
                                            <span class="icon text-white">
                                            <i class="fas fa-trash"></i>
                                            </span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection