@extends('layouts.app')

@section('title', 'Form Kelas')

@section('content')

<div class="container-fluid">
  
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-8 mb-4">
            <div class="card">
                <div class="card-header">
                    Form Kelas
                </div>
                <div class="card-body">
                    <form action="{{ (!isset($kelas->id)) ?  route('kelas.store') : route('kelas.update', $kelas->id) }}" method="post">
                        @csrf
        
                        @if(isset($kelas->id))
                            @method('put')
                        @endif
        
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Nama Kelas</label>
                                <input type="text" class="form-control form-control-user" name="nama_kelas" value="{{ old('nama_kelas', $kelas->nama_kelas) }}">
                                @error('nama_kelas')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">                            
                                <label>Tipe Kelas</label>
                                <select name="tipe_kelas" class="form-control">
                                    <option value=""> - Pilih Tipe Kelas -</option>
                                    <option value="a" {{ (old('tipe_kelas') == 'a' || $kelas->tipe_kelas == 'a') ? 'selected' : '' }}>A</option>
                                    <option value="b" {{ (old('tipe_kelas') == 'b' || $kelas->tipe_kelas == 'b') ? 'selected' : '' }}>B</option>                                    
                                </select>
                                @error('tipe_kelas')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                           
                            </div>
                        </div>  
                        
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Tahun Ajar</label>
                                <input type="text" class="form-control form-control-user" name="tahun_ajar" value="{{ old('tahun_ajar', $kelas->tahun_ajar) }}">
                                @error('tahun_ajar')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Keterangan</label>
                                <textarea class="form-control form-control-user" name="keterangan">{{ old('keterangan', $kelas->keterangan) }}</textarea>
                                @error('keterangan')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">                            
                                <label>Status Kelas</label>
                                <select name="status" class="form-control">
                                    <option value=""> - Pilih Status -</option>
                                    <option value="1" {{ (old('status') == '1' || $kelas->status == '1') ? 'selected' : '' }}>Aktif</option>
                                    <option value="0" {{ (old('status') == '0' || $kelas->status == '0') ? 'selected' : '' }}>Non Aktif</option>                                    
                                </select>
                                @error('status')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                           
                            </div>
                        </div>  
                        
                        <button type="submit" class="btn btn-warning btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-save"></i>
                            </span>
                            <span class="text">Simpan</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection