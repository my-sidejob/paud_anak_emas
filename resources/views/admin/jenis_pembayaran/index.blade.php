@extends('layouts.app')

@section('title', 'Jenis Pembayaran')

@section('content')

<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    Data Master Jenis Pembayaran
                </div>
                <div class="card-body">
                    @if(Auth::guard('web')->check())
                    @if(Auth::guard('web')->user()->level == 2)
                    <a href="{{ route('jenis-pembayaran.create') }}" class="btn btn-warning mb-4 btn-sm"><i class="fa fa-plus"></i> Tambah</a>
                    @endif
                    @endif
                    <table class="table table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Pembayaran</th>
                                <th>Biaya</th>                                
                                <th>Keterangan</th>
                                <th>Status</th>
                                @if(Auth::guard('web')->check())
                                @if(Auth::guard('web')->user()->level == 2)
                                <th></th>                                
                                @endif
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($jenis_pembayaran as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ ucwords($row->nama_pembayaran) }}</td>
                                <td>{{ number_format($row->biaya) }}</td>
                                <td>{{ $row->keterangan }}</td>
                                <td>{!! $row->getStatusBadge() !!}</td>
                                @if(Auth::guard('web')->check())
                                @if(Auth::guard('web')->user()->level == 2)
                                <td>
                                    <form action="{{ route('jenis-pembayaran.destroy', $row->id) }}" method="post">                                       
                                        <a href="{{ route('jenis-pembayaran.edit', $row->id) }}" class="btn btn-sm btn-warning" title="Edit"><i class="fa fa-cog"></i></a>
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Yakin hapus data?')" >
                                            <span class="icon text-white">
                                            <i class="fas fa-trash"></i>
                                            </span>
                                        </button>
                                    </form>
                                </td>
                                @endif
                                @endif
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection


@push('scripts')
    <script>
        $('.sidebar-pembayaran').removeClass('active');
    </script>
@endpush