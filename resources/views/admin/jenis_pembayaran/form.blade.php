@extends('layouts.app')

@section('title', 'Form Jenis Pembayaran')

@section('content')

<div class="container-fluid">
  
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-8 mb-4">
            <div class="card">
                <div class="card-header">
                    Form Jenis Pembayaran
                </div>
                <div class="card-body">
                    <form action="{{ (!isset($jenis_pembayaran->id)) ?  route('jenis-pembayaran.store') : route('jenis-pembayaran.update', $jenis_pembayaran->id) }}" method="post">
                        @csrf
        
                        @if(isset($jenis_pembayaran->id))
                            @method('put')
                        @endif
        
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Nama Jenis Pembayaran</label>
                                <input type="text" class="form-control form-control-user" name="nama_pembayaran" value="{{ old('nama_pembayaran', $jenis_pembayaran->nama_pembayaran) }}">
                                @error('nama_pembayaran')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                
                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Biaya</label>
                                <input type="number" class="form-control form-control-user" name="biaya" value="{{ old('biaya', $jenis_pembayaran->biaya) }}">
                                @error('biaya')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">
                                <label>Keterangan</label>
                                <textarea class="form-control form-control-user" name="keterangan">{{ old('keterangan', $jenis_pembayaran->keterangan) }}</textarea>
                                @error('keterangan')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12 mb-2">                            
                                <label>Status</label>
                                <select name="status" class="form-control">
                                    <option value=""> - Pilih Status -</option>
                                    <option value="1" {{ (old('status') == '1' || $jenis_pembayaran->status == '1') ? 'selected' : '' }}>Aktif</option>
                                    <option value="0" {{ (old('status') == '0' || $jenis_pembayaran->status == '0') ? 'selected' : '' }}>Non Aktif</option>                                    
                                </select>
                                @error('status')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror                           
                            </div>
                        </div>  
                        
                        <button type="submit" class="btn btn-warning btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-save"></i>
                            </span>
                            <span class="text">Simpan</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection