<div class="modal-header">
    <h5 class="modal-title">Form Nilai Siswa</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form action="{{ (!isset($nilai->id)) ?  route('nilai.store', [$siswa_id, $kelas_id]) : route('nilai.update', $nilai->id) }}" method="post">
    @csrf
    @if(isset($nilai->id))
        @method('put')
    @endif
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group row custom-fields">
                    <div class="col-sm-12 mb-2 text-left">
                        <input type="hidden" name="kelas_id" value="{{ $kelas_id }}">
                        <input type="hidden" name="siswa_id" value="{{ $siswa_id }}">
                        <label>Kegiatan / Pelajaran</label>
                        <select name="kegiatan_id" class="form-control mySelect" style="text-align: left">
                            <option value=""> - Pilih Kegiatan / Pelajaran -</option>
                            @foreach($kegiatan as $option)
                                <option value="{{ $option->kegiatan->id }}" {{ (old('kegiatan_id') == $option->kegiatan->id || $nilai->kegiatan_id == $option->kegiatan->id) ? 'selected' : '' }}>{{ ucwords($option->kegiatan->nama_kegiatan) }}</option>
                            @endforeach
                        </select>
                        @error('kegiatan_id')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror                           
                    </div>
                </div>

                <div class="form-group row custom-fields text-left">
                    <div class="col-12">
                        <label>Catatan Penilaian</label>
                        <textarea name="catatan_penilaian" class="form-control" required>{{ $nilai->catatan_penilaian }}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-warning">Simpan</button>
    </div>
</form>


<script>
    $(document).ready( function () {
        //select2 input option
        $('.mySelect').select2();
    } );
  </script>