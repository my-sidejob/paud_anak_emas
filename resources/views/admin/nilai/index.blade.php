@extends('layouts.app')

@section('title', 'Nilai Siswa')

@section('content')

<div class="container-fluid">
    <div class="row mb-4">
        <div class="col-md-12">
            <a href="{{ route('kelas-siswa.index') .'?kelas_id=' . $kelas->id   }}"><i class="fa fa-arrow-left"></i> Kembali </a>
        </div>
    </div>
    
    <div class="row mb-4">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    Detail Kelas
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <tr>
                                    <th style="border-top:0">Nama Kelas</th>
                                    <th style="border-top:0">:</th>
                                    <td style="border-top:0">{{ ucwords($kelas->nama_kelas) }}</td>
                                </tr>
                                <tr>
                                    <th>Tipe Kelas</th>
                                    <th>:</th>
                                    <td>{{ ucwords($kelas->tipe_kelas) }}</td>
                                </tr>
                                <tr>
                                    <th>Tahun Ajar</th>
                                    <th>:</th>
                                    <td>{{ $kelas->tahun_ajar }}</td>
                                </tr>
                                <tr>
                                    <th>Status Kelas</th>
                                    <th>:</th>
                                    <td>{!! $kelas->getStatusBadge() !!}</td>
                                </tr>
                                <tr>
                                    <th>Keterangan</th>
                                    <th>:</th>
                                    <td>{{ $kelas->keterangan }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <hr>
                    
                    <form action="{{ route('siswa.changeStatus', $siswa->id) }}" method="post">
                        @csrf 
                        <div class="form-group row">                            
                            <div class="col-sm-6 mb-2">                            
                                <select name="status" class="form-control" required>
                                    <option value=""> - Status Siswa -</option>
                                    <option value="2">Aktif</option>
                                    @if($kelas->tipe_kelas == 'a')
                                    <option value="3">Nyatakan Lulus</option>
                                    @else 
                                    <option value="4">Nyatakan Lulus</option>
                                    @endif
                                    <option value="5">Nyatakan Tidak Lulus</option>                                    
                                </select>                          
                            </div>
                            <div class="col-md-6">
                                <input type="submit" value="Simpan" class="btn btn-warning">
                            </div>
                        </div>  
                    </form>
                        
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    Identitas Siswa
                </div>
                <div class="card-body">
                    <table class="table">
                        <tr>
                            <th style="border-top:0">Nama Siswa</th>
                            <th style="border-top:0">:</th>
                            <td style="border-top:0">{{ ucwords($siswa->nama_lengkap) }}</td>
                        </tr>
                        <tr>
                            <th>TTL</th>
                            <th>:</th>
                            <td>{{ ucwords($siswa->tempat_lahir) . ', ' . $siswa->tanggal_lahir }}</td>
                        </tr>
                        <tr>
                            <th>Umur</th>
                            <th>:</th>
                            <td>{{ $siswa->getAge() }} Tahun</td>
                        </tr>
                        <tr>
                            <th>Jenis Kelamin</th>
                            <th>:</th>
                            <td>{!! $siswa->getJenisKelamin() !!}</td>
                        </tr>
                        <tr>
                            <th>Status Siswa</th>
                            <th>:</th>
                            <td>{!! $siswa->getStatusBadge() !!}</td>
                        </tr>
                        <tr>
                            <th>Orangtua</th>
                            <th>:</th>
                            <td>{{ ucwords($siswa->orangtua->nama_lengkap) }}</td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>
        
    </div>

    <div class="row mb-4">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    Table Nilai
                </div>
                <div class="card-body">
                    <a href="{{ route('nilai.create', [$siswa->id, $kelas->id]) }}" class="btn btn-sm btn-primary mb-4 btn-nilai"><i class="fa fa-plus"></i> Tambah Nilai</a>
                    <table class="table table-sm" id="myTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Kegiatan / Pelajaran</th>
                                <th>Sentra</th>
                                <th>Penilaian</th>
                                <th>Guru</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($nilai as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->kegiatan->nama_kegiatan }}</td>
                                <td>{{ $row->kegiatan->sentra->nama_sentra }}</td>
                                <td>{{ $row->catatan_penilaian }}</td>
                                <td>{{ ucwords($row->user->nama_lengkap) }}</td>
                                <td>
                                    <form action="{{ route('nilai.destroy', $row->id) }}" method="post">                                       
                                        <a href="{{ route('nilai.edit', $row->id) }}" class="btn btn-sm btn-warning btn-nilai" title="Edit"><i class="fa fa-cog"></i></a>
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Yakin hapus data?')" >
                                            <span class="icon text-white">
                                            <i class="fas fa-trash"></i>
                                            </span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="theModal-lg" class="modal fade text-center">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
        </div>
    </div>
</div>

@endsection


@push('scripts')
<script>
    $('.btn-nilai').on('click', function(e){
        e.preventDefault();
        $('#theModal-lg').modal('show').find('.modal-content').load($(this).attr('href'));
    });

    $('.sidebar-link').removeClass('active');
    $('.sidebar-pembelajaran').addClass('active');
</script>


@endpush