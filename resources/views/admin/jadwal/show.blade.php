@extends('layouts.app')

@section('title', 'Jadwal')

@section('content')

<div class="container-fluid">
    <div class="row mb-4">
        <div class="col-md-12">
            <a href="{{ route('kelas-siswa.index') .'?kelas_id=' . $kelas->id   }}"><i class="fa fa-arrow-left"></i> Kembali </a>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header font-weight-bold text-primary">
                    Jadwal Kelas {{ ucwords($kelas->nama_kelas). ' ' .ucwords($kelas->tipe_kelas) . ' ' . $kelas->tahun_ajar }}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('jadwal.create') .'?kelas_id=' . $kelas->id }}" class="btn btn-secondary btn-sm btn-jadwal mb-3">Tambah Jadwal</a>
                        </div>
                    </div>
                    @foreach($jadwal as $week)
                        <hr style="margin-bottom: 2.3rem!important;">
                        <div class="row justify-content-center">                        
                            @foreach($week->getJadwalOfWeek() as $row)
                            <div class="col-md-6 mb-4">
                                <div class="card">
                                    <div class="card-header font-weight-bold text-primary">                                        
                                        {{ $row->getFormatTanggal() }}
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-sm table-jadwal">
                                            <tr>
                                                <th style="border-top:0">Nama</th>
                                                <th style="border-top:0">Sentra</th>
                                                <th style="border-top:0; min-width: 105px;">Jam</th>
                                            </tr>
                                            @foreach($row->getJadwalOfDate() as $jad)
                                            <tr>
                                                <td>{{ $jad->kegiatan->nama_kegiatan }}</td>
                                                <td>{{ $jad->kegiatan->sentra->nama_sentra }}</td>
                                                <td>{{ $jad->jam_mulai .' - '. $jad->jam_selesai }}</td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @endforeach                                    
                        </div>
                        
                    @endforeach
                    <br>
                    
                </div>
            </div>
        </div>
    </div>
</div>


<div id="theModal-lg" class="modal fade text-center">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
        </div>
    </div>
</div>

@endsection

@push('scripts')

<script>
    $('.btn-jadwal').on('click', function(e){
        e.preventDefault();
        $('#theModal-lg').modal('show').find('.modal-content').load($(this).attr('href'));
    });

    $('.sidebar-link').removeClass('active');
    $('.sidebar-pembelajaran').addClass('active');
</script>

@endpush