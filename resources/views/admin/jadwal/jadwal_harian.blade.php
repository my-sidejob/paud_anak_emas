<div class="modal-header">
    <h5 class="modal-title">Detail Jadwal</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form action="{{ route('jadwal.store') }}" method="post">
    @csrf
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table text-left">
                    <tr>
                        <th style="border-top:0">Hari / Tanggal</th>
                        <th style="border-top:0">Sentra</th>
                        <td style="border-top:0">Jam</td>
                        <th style="border-top:0">Kegiatan / Pelajaran</td>
                    </tr>
                    @foreach($jadwal as $row)
                    <tr>
                        <td>{{ $row->getFormatTanggal() }}</td>
                        <td>{{ $row->kegiatan->sentra->nama_sentra }}</td>
                        <td>{{ $row->jam_mulai .' - '. $row->jam_selesai }}</td>
                        <td>{{ $row->kegiatan->nama_kegiatan }}</td>
                    </tr>
                    @endforeach
                    
                </table>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
</form>


<script>
    $(document).ready( function () {
        //select2 input option
        $('.mySelect').select2();
    } );

    $( function() {
      $( ".datepicker" ).datepicker({ 
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
      });
    } );

  </script>