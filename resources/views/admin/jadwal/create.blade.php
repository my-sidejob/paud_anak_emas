<div class="modal-header">
    <h5 class="modal-title">Tambah Jadwal Pada Kelas Ini</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form action="{{ route('jadwal.store') }}" method="post">
    @csrf
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group row">
                    <div class="col-md-12 text-left">
                        <label>Tanggal</label>
                        <input type="text" name="tanggal" class="form-control datepicker" placeholder="Masukan Tanggal" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 text-left">
                        <label>Jam Mulai</label>
                        <input type="time" name="jam_mulai" required>
                    </div>
                    <div class="col-md-6 text-left">
                        <label>Jam Selesai</label>
                        <input type="time" name="jam_selesai" required>
                    </div>
                </div>
                <div class="form-group row custom-fields">
                    <div class="col-sm-12 mb-2 text-left">
                        <label>Kegiatan / Pelajaran</label>
                        <input type="hidden" name="kelas_id" value="{{ $kelas_id }}">
                        <select name="kegiatan_id" class="form-control mySelect" style="text-align: left" required>
                            <option value=""> - Pilih Kegiatan / Pelajaran -</option>
                            @foreach($kegiatan as $option)
                                <option value="{{ $option->id }}" {{ (old('kegiatan_id') == $option->id) ? 'selected' : '' }}>{{ ucwords($option->nama_kegiatan) }}</option>
                            @endforeach
                        </select>
                        @error('kegiatan_id')
                            <span class="invalid-feedback d-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror                           
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12 text-left">
                        <label>Keterangan</label>
                        <textarea name="keterangan" class="form-control" placeholder="Masukan keterangan (opsional)"></textarea>
                    </div>
                </div>         
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-warning">Simpan</button>
    </div>
</form>


<script>
    $(document).ready( function () {
        //select2 input option
        $('.mySelect').select2();
    } );

    $( function() {
      $( ".datepicker" ).datepicker({ 
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
      });
    } );

  </script>