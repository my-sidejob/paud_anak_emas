<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sentra extends Model
{
    protected $table = 'Sentra';

    protected $fillable = [
        'nama_sentra',
        'keterangan',
    ];

    public static function getDefaultValues()
    {
        return (object) [
            'nama_sentra' => '',
            'keterangan' => ''
        ];
    }

    public function kegiatan()
    {
        return $this->hasMany('App\Kegiatan');
    }
}
