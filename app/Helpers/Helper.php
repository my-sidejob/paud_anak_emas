<?php

use App\Produk;

function urlHasPrefix(string $prefix) {
    $url = url()->current();
    if (strpos($url, $prefix) > 0) {
        return true;
    }

    return false;
}


function getProductDetail($id)
{
    return Produk::find($id);
}

function showStatus($number)
{
    if($number == 1){
        return 'Masuk';
    }

    if($number == 2){
        return 'Telah Dikonfirmasi';
    }

    if($number == 3){
        return 'Sedang Diproses';
    }

    if($number == 4){
        return 'Selesai';
    }

    if($number == 5){
        return 'Telah Diambil';
    }

    return '';
    // if($number == 6){
    //     return 'Batal';
    // }
}

function listStatusPemesanan()
{
    return [
        '1' => 'Menunggu Konfirmasi',
        '2' => 'Pesanan Telah Dikonfirmasi',
        '3' => 'Pesanan Sedang Diproses',
        '4' => 'Pesanan Selesai',
        '5' => 'Pesanan Telah Diambil',
    ];
}