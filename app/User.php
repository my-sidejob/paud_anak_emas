<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_lengkap', 'username', 'email', 'password', 'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin', 'level'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($val)
    {
        return $this->attributes['password'] = bcrypt($val);
    }


    public static function getDefaultValues()
    {
        return (object) [
            'nama_lengkap' => '', 
            'username' => '', 
            'email' => '', 
            'password' => '', 
            'tempat_lahir' => '',
            'tanggal_lahir' => '',
            'jenis_kelamin' => '',
            'level' => ''
        ];
    }

    public function getLevelBadge()
    {
        if($this->level == 1){
            return "<span class='badge badge-success'>Guru</span>";
        } else if($this->level == 2){
            return "<span class='badge badge-primary'>Admin</span>";
        }
    }

    public function getJenisKelamin()
    {
        if($this->jenis_kelamin == 'l'){
            return 'Laki - laki';
        } else {
            return 'Perempuan';
        }
    }
}
