<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Jadwal extends Model
{
    protected $table = 'jadwal';

    protected $fillable = [
        'tanggal',
        'jam_mulai',
        'jam_selesai',
        'keterangan',
        'kelas_id',
        'sentra_id',
        'kegiatan_id'        
    ];

    public function getFormatTanggal()
    {
        return __('strings.'.Carbon::parse($this->tanggal)->format('l')) . ', ' . Carbon::parse($this->tanggal)->format('Y-m-d');
    }

    public function getHariTanggal()
    {
        return Carbon::parse($this->tanggal)->format('l');
    }

    public function kegiatan()
    {
        return $this->belongsTo('App\Kegiatan');
    }

    public function getJadwalOfWeek()
    {
        $q = Jadwal::select('kelas_id', 'tanggal')->whereRaw("week(tanggal) = $this->week")->where('kelas_id', $this->kelas_id)->groupBy('kelas_id', 'tanggal')->get();
        return $q;
    }

    public function getJadwalOfDate()
    {
        $q = Jadwal::where('kelas_id', $this->kelas_id)->where('tanggal', $this->tanggal)->orderBy('jam_mulai', 'asc')->get();
        return $q;
    }
}

