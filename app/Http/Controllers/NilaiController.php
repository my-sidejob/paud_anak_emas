<?php

namespace App\Http\Controllers;

use App\Jadwal;
use App\Kelas;
use App\Nilai;
use App\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NilaiController extends Controller
{
    public function index($siswa_id, $kelas_id)
    {
        $data['siswa'] = Siswa::find($siswa_id);
        $data['kelas'] = Kelas::find($kelas_id);
        $data['nilai'] = Nilai::getNilaiSiswa($siswa_id, $kelas_id);
        return view('admin.nilai.index', $data);
    }

    public function create($siswa_id, $kelas_id)
    {
        $data['nilai'] = Nilai::getDefaultValues();
        $data['siswa_id'] = $siswa_id;
        $data['kelas_id'] = $kelas_id;
        $data['kegiatan'] = Jadwal::selectRaw("distinct(kegiatan_id)")->join('kegiatan', 'kegiatan.id', '=', 'jadwal.kegiatan_id')->where('kelas_id', $kelas_id)->orderBy('nama_kegiatan', 'asc')->get();
        return view('admin.nilai.modal', $data);
    }

    public function store(Request $request)
    {
        $request['user_id'] = Auth::guard('web')->user()->id;
        Nilai::create($request->toArray());
        return redirect()->back()->with('success', 'Berhasil menambah nilai siswa');
    }

    public function edit($id)
    {
        $data['nilai'] = Nilai::find($id);
        $data['siswa_id'] = $data['nilai']->siswa_id;
        $data['kelas_id'] = $data['nilai']->kelas_id;
        $data['kegiatan'] = Jadwal::selectRaw("distinct(kegiatan_id)")->join('kegiatan', 'kegiatan.id', '=', 'jadwal.kegiatan_id')->where('kelas_id', $data['kelas_id'])->orderBy('nama_kegiatan', 'asc')->get();
        return view('admin.nilai.modal', $data);
    }

    public function update(Request $request, $id)
    {
        $request['user_id'] = Auth::guard('web')->user()->id;
        Nilai::find($id)->update($request->toArray());
        return redirect()->back()->with('success', 'Berhasil mengubah nilai siswa');
    }

    public function delete($id)
    {
        Nilai::find($id)->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus nilai siswa');
    }
}
