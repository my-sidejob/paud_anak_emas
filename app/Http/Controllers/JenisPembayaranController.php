<?php

namespace App\Http\Controllers;

use App\JenisPembayaran;
use Illuminate\Http\Request;

class JenisPembayaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('admin-only')->except('index');
    }


    public function index()
    {
        $data['jenis_pembayaran'] = JenisPembayaran::orderBy('nama_pembayaran', 'asc')->orderBy('status', 'desc')->get();
        return view('admin.jenis_pembayaran.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['jenis_pembayaran'] = JenisPembayaran::getDefaultValues();
        return view('admin.jenis_pembayaran.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_pembayaran' => 'required',
            'biaya' => 'required',
            'keterangan' => 'required',
            'status' => 'required',
        ]);
        JenisPembayaran::create($request->all());
        return redirect()->route('jenis-pembayaran.index')->with('success', 'Berhasil menambah data sentra');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jenis_pembayaran = JenisPembayaran::find($id);
        return view('admin.jenis_pembayaran.form', compact('jenis_pembayaran'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_pembayaran' => 'required',
            'biaya' => 'required',
            'keterangan' => 'required',
            'status' => 'required',
        ]);
        
        $jenis_pembayaran = JenisPembayaran::find($id);

        $jenis_pembayaran->update($request->all());

        return redirect()->route('jenis-pembayaran.index')->with('success', 'Berhasil mengubah data sentra');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jenis_pembayaran = JenisPembayaran::find($id);

        $jenis_pembayaran->delete();

        return redirect()->route('jenis-pembayaran.index')->with('success', 'Berhasil menghapus data sentra');
    }
}
