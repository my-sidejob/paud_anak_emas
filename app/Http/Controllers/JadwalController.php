<?php

namespace App\Http\Controllers;

use App\Jadwal;
use App\Kegiatan;
use App\Kelas;
use Illuminate\Http\Request;

class JadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data['kelas_id'] = $request->kelas_id;
        $data['kegiatan'] = Kegiatan::orderBy('nama_kegiatan', 'asc')->get();
        return view('admin.jadwal.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kegiatan_id' => 'required',
            'jam_mulai' => 'required',
            'jam_selesai' => 'required',
            'tanggal' => 'required',
            'kelas_id' => 'required'
        ]);

        $kegiatan = Kegiatan::find($request->kegiatan_id);
        $request['sentra_id'] = $kegiatan->sentra_id;

        Jadwal::create($request->toArray());
        
        return redirect()->back()->with('success', 'Berhasil menambah data jadwal pada kelas.');

    }

    public function jadwalHarian($kelas_id, $tanggal)
    {
        $data['jadwal'] = Jadwal::where('kelas_id', $kelas_id)->where('tanggal', $tanggal)->orderBy('jam_mulai', 'asc')->get();
        return view('admin.jadwal.jadwal_harian', $data);
    }

    public function showJadwalKelas($kelas_id)
    {
        $data['jadwal'] = Jadwal::selectRaw("week(tanggal) AS week, kelas_id")->where('kelas_id', $kelas_id)->groupByRaw("week(tanggal), kelas_id")->orderBy('tanggal', 'asc')->get();       
        $data['kelas'] = Kelas::find($kelas_id);
        return view('admin.jadwal.show', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
