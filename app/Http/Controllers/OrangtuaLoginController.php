<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrangtuaLoginController extends Controller
{
    use AuthenticatesUsers;
    
    protected $redirectTo = '/ortu/dashboard';

    public function __construct()
    {
        $this->middleware('guest:orangtua')->except('logout')->except('index');
    }
    
    protected function guard()
    {
        return Auth::guard('orangtua');
    }

    public function username()
    {
        return 'username';
    }
}
