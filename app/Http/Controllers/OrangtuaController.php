<?php

namespace App\Http\Controllers;

use App\Orangtua;
use Illuminate\Http\Request;

class OrangtuaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data['orangtua'] = Orangtua::orderBy('nama_lengkap', 'asc')->get();
        return view('admin.orangtua.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['orangtua'] = Orangtua::getDefaultValues();
        return view('admin.orangtua.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_lengkap' => 'required',
            'username' => 'required|unique:orangtua',
            'password' => 'required',
            'email' => 'required|unique:orangtua',
            'no_telp' => 'required',
            'alamat' => 'required'
        ]);
        Orangtua::create($request->all());
        return redirect()->route('orangtua.index')->with('success', 'Berhasil menambah data orangtua');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orangtua = Orangtua::find($id);
        return view('admin.orangtua.modal', compact('orangtua'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $orangtua = Orangtua::find($id);
        return view('admin.orangtua.form', compact('orangtua'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_lengkap' => 'required',
            'username' => 'required|unique:orangtua,username,'.$id,
            'email' => 'required|unique:orangtua,email,'.$id,
            'no_telp' => 'required',
            'alamat' => 'required'
        ]);
        
        $orangtua = Orangtua::find($id);

        $orangtua->update($request->all());

        return redirect()->route('orangtua.index')->with('success', 'Berhasil mengubah data orangtua');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $orangtua = Orangtua::find($id);

        $orangtua->delete();

        return redirect()->route('orangtua.index')->with('success', 'Berhasil menghapus data orangtua');
    }
}
