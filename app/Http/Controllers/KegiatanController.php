<?php

namespace App\Http\Controllers;

use App\Kegiatan;
use App\Sentra;
use Illuminate\Http\Request;

class KegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['kegiatan'] = Kegiatan::orderBy('nama_kegiatan', 'asc')->get();
        return view('admin.kegiatan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['kegiatan'] = Kegiatan::getDefaultValues();
        $data['sentra'] = Sentra::orderBy('nama_sentra')->get();
        return view('admin.kegiatan.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_kegiatan' => 'required',
            'sentra_id' => 'required'
        ]);
        Kegiatan::create($request->all());
        return redirect()->route('kegiatan.index')->with('success', 'Berhasil menambah data sentra');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['kegiatan'] = Kegiatan::find($id);
        $data['sentra'] = Sentra::orderBy('nama_sentra')->get();
        return view('admin.kegiatan.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_kegiatan' => 'required',
            'sentra_id' => 'required'
        ]);
        
        $kegiatan = Kegiatan::find($id);

        $kegiatan->update($request->all());

        return redirect()->route('kegiatan.index')->with('success', 'Berhasil mengubah data sentra');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kegiatan = Kegiatan::find($id);

        $kegiatan->delete();

        return redirect()->route('kegiatan.index')->with('success', 'Berhasil menghapus data sentra');
    }
}
