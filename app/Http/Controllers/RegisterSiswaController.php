<?php

namespace App\Http\Controllers;

use App\Orangtua;
use App\Siswa;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class RegisterSiswaController extends Controller
{
    use AuthenticatesUsers;
    
    protected $redirectTo = '/ortu/dashboard';

    public function __construct()
    {
        $this->middleware('guest:orangtua')->except('logout')->except('index');
    }

    public function index()
    {
        $data['field'] = Siswa::getDefaultValues('register');
        $data['orangtua'] = Orangtua::getDefaultValues();
        return view('register_siswa', $data);
    }

    protected function guard()
    {
        return Auth::guard('orangtua');
    }

    public function store(Request $request)
    {
        $request->validate([
            "nama_lengkap_anak" => 'required',
            "tempat_lahir" => 'required',
            "tanggal_lahir" => 'required',
            "jenis_kelamin" => 'required',
            "jenis_kelompok" => 'required',
            "tahun_ajar" => 'required',
            "nama_lengkap" => 'required',
            "username" => 'required|unique:orangtua',
            "password" => 'required',
            "email" => 'required|unique:orangtua',
            "no_telp" => 'required',
            "alamat" => 'required',
        ]);

        //store table orangtua
        $orangtua = Orangtua::create([
            "nama_lengkap" => $request->nama_lengkap,
            "username" => $request->username,
            "password" => $request->password,
            "email" => $request->email,
            "no_telp" => $request->no_telp,
            "alamat" => $request->alamat,
        ]);

        $siswa = Siswa::create([
            "nama_lengkap" => $request->nama_lengkap_anak,
            "tempat_lahir" => $request->tempat_lahir,
            "tanggal_lahir" => $request->tanggal_lahir,
            "jenis_kelamin" => $request->jenis_kelamin,
            "status" => 0,
            "orangtua_id" => $orangtua->id,
            "keterangan" => 'Didaftarkan pada kelompok: '.  ucwords($request->jenis_kelompok) . ', tahun ajar ' .$request->tahun_ajar,
        ]);
        
        Auth::guard('orangtua')->login($orangtua);

        return redirect()->route('ortu.dashboard')->with('success', 'Berhasil mendaftarkan anak. Sekarang anda dapat login menggunakan username dan password yang telah anda isi pada form pendaftaran siswa. Sekarang silahkan lengkapi pembayaran pada menu pembayaran agar pendaftaran diterima dan dikonfirmasi oleh petugas');
        
    }
}
