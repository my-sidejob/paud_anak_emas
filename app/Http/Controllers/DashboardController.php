<?php

namespace App\Http\Controllers;

use App\Kegiatan;
use App\Sentra;
use App\Siswa;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function indexOrtu()
    {        
        return view('dashboard');
    }

    public function index()
    {
        $data['total_siswa'] = Siswa::all()->count();
        $data['total_kegiatan'] = Kegiatan::all()->count();
        $data['total_admin_guru'] = User::all()->count();
        $data['total_sentra'] = Sentra::all()->count();
        return view('admin.dashboard', $data);
    }
}
