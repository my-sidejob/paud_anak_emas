<?php

namespace App\Http\Controllers;

use App\Orangtua;
use App\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['siswa'] = Siswa::orderBy('nama_lengkap', 'asc')->get();
        return view('admin.siswa.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['siswa'] = Siswa::getDefaultValues();
        $data['orangtua'] = Orangtua::orderBy('nama_lengkap')->get();
        return view('admin.siswa.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_lengkap' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'orangtua_id' => 'required'
        ]);
        $request['status'] = '1';

        Siswa::create($request->toArray());
        return redirect()->route('siswa.index')->with('success', 'Berhasil menambah data siswa');
    }
    
    public function changeStatus(Request $request, $siswa_id)
    {
        $input = $request->toArray();

        if($input['status'] == 3 || $input['status'] == 4){
            $text = 'menyatakan lulus';
        } else if($input['status'] == 2) {
            $text = 'menyatakan aktif';
        } else {
            $text = 'menyatakan tidak lulus';
        }

        Siswa::find($siswa_id)->update(['status' => $input['status']]);

        return redirect()->back()->with('success', 'Berhasil ' . $text . ' siswa ini');
    }


    
    public function daftarBaru()
    {
        return view('orangtua.daftar_baru');
    }

    public function simpanDaftar(Request $request)
    {
        $request->validate([
            'nama_lengkap' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'tahun_ajar' => 'required',
        ]);

        $input = $request->toArray();

        $input['orangtua_id'] = Auth::guard('orangtua')->user()->id;

        $input['status'] = 0;

        $input['keterangan'] = 'Didaftarkan pada kelompok: '.  ucwords($request->jenis_kelompok) . ', tahun ajar ' .$request->tahun_ajar;

        Siswa::create($input);

        return redirect()->back()->with('success', 'Berhasil mendaftarkan anak. Sekarang silahkan lengkapi pembayaran pada menu pembayaran agar pendaftaran diterima dan dikonfirmasi oleh petugas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $siswa = Siswa::find($id);
        return view('admin.siswa.modal', compact('siswa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['siswa'] = Siswa::find($id);
        $data['orangtua'] = Orangtua::orderBy('nama_lengkap')->get();
        return view('admin.siswa.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_lengkap' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'orangtua_id' => 'required'
        ]);
        
        $siswa = Siswa::find($id);

        $siswa->update($request->all());

        return redirect()->route('siswa.index')->with('success', 'Berhasil mengubah data siswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $siswa = Siswa::find($id);

        $siswa->delete();

        return redirect()->route('siswa.index')->with('success', 'Berhasil menghapus data siswa');
    }
}
