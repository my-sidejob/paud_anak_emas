<?php

namespace App\Http\Controllers;

use App\Jadwal;
use App\Kelas;
use App\Siswa;
use Illuminate\Http\Request;

class KelasSiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->kelas_id)){
            $data['kelas'] = Kelas::find($request->kelas_id);
            $data['jadwal'] = Jadwal::select('tanggal')->where('kelas_id', $request->kelas_id)->groupBy('tanggal')->orderBy('tanggal', 'desc')->limit(5)->get();
            
            return view('admin.kelas_siswa.show', $data);
        } else {
            $data['kelas'] = Kelas::orderBy('nama_kelas', 'asc')->get();
            return view('admin.kelas_siswa.index', $data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $data['kelas_id'] = $_GET['kelas_id'];
        $data['siswa'] = Siswa::where('status', 1)->get();
        return view('admin.kelas_siswa.modal', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'siswa_id' => 'required',
            'kelas_id' => 'required'
        ]);

        $kelas = Kelas::find($request->kelas_id);
        if($kelas->status == 0){
            return redirect()->back()->with('error', 'Tidak dapat menambah siswa pada kelas yang tidak aktif');
        }
        Siswa::find($request->siswa_id)->update(['status' => 2]);
        $kelas->kelasSiswa()->attach($request->siswa_id);
        return redirect()->back()->with('success', 'Berhasil menambah data siswa pada kelas tersebut');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $kelas = Kelas::find($id);
        Siswa::find($request->siswa_id)->update(['status' => 1]);
        $kelas->kelasSiswa()->detach($request->siswa_id);

        return redirect()->back()->with('success', 'Berhasil menghapus siswa dari kelas');
    }
}
