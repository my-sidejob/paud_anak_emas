<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class KritikSaranController extends Controller
{
    public function create()
    {
        return view('orangtua.form_kritik_saran');
    }

    public function store(Request $request)
    {
        $request->validate([
            'isi' => 'required',
        ]);

        $input = $request->toArray();
      

        DB::table('kritik_saran')->insert([
            'tanggal' => date('Y-m-d'),
            'orangtua_id' => Auth::guard('orangtua')->user()->id,
            'isi' => $input['isi'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        return redirect()->back()->with('success', 'Berhasil mengirim kritik dan saran');
    }

    public function index()
    {
        $data['kritik_saran'] = DB::table('kritik_saran')->join('orangtua', 'orangtua.id', '=', 'kritik_saran.orangtua_id')->orderBy('tanggal', 'desc')->get();
        return view('admin.kritik_saran', $data);
    }
}
