<?php

namespace App\Http\Controllers;

use App\JenisPembayaran;
use App\Pembayaran;
use App\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PembayaranController extends Controller
{
    public function jenisPembayaran()
    {
        $data['jenis_pembayaran'] = JenisPembayaran::orderBy('nama_pembayaran', 'asc')->orderBy('status', 'desc')->get();
        return view('admin.jenis_pembayaran.index', $data);
    }

    public function indexOrtu()
    {
        $data['siswa'] = Siswa::where('orangtua_id', Auth::guard('orangtua')->user()->id)->whereRaw("status not in (3, 4, 5)")->get();
        $data['jenis_pembayaran'] = JenisPembayaran::where('status', 1)->orderBy('nama_pembayaran', 'asc')->orderBy('status', 'desc')->get();
        return view('orangtua.form_pembayaran', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'siswa_id' => 'required',
            'jenis_pembayaran_id' => 'required',
            'bukti_transfer' => 'required',        
        ]);

        $input = $request->toArray();

        $input['tanggal_pembayaran'] = date('Y-m-d');
        $input['status'] = 0;

        //upload file
        $request->file('bukti_transfer')->store('bukti_transfer');

        $input['bukti_pembayaran'] = $request->bukti_transfer->hashName();

        Pembayaran::create($input);

        return redirect()->route('ortu.riwayat-pembayaran')->with('success', 'Berhasil melakukan pembayaran, silahkan menunggu konfirmasi dari petugas mengenai pembayaran anda');
    }

    public function riwayatPembayaran()
    {
        $siswa = Siswa::where('orangtua_id', Auth::guard('orangtua')->user()->id)->get()->pluck('id')->all();    
        $data['pembayaran'] = Pembayaran::whereIn('siswa_id', $siswa)->orderBy('tanggal_pembayaran', 'desc')->get();    
        return view('orangtua.riwayat_pembayaran', $data);
    }


    public function index()
    {
        $data['pembayaran'] = Pembayaran::orderBy('created_at', 'desc')->orderBy('status', 'asc')->get();
        return view('admin.pembayaran.index', $data);
    }

    public function changeStatus($id, $status)
    {
        $pembayaran = Pembayaran::find($id);
        $siswa = Siswa::find($pembayaran->siswa_id);

        if($siswa->status == 0 && $status == 1){
            $siswa->update(['status' => 1, 'keterangan' => null]);
        }

        $pembayaran->update(['status' => $status]);

        if($status == 1){
            $text = 'menerima';
        } else {
            $text = 'menolak';
        }
        
        return redirect()->back()->with('success', 'Berhasil '.$text . ' pembayaran');
    }
}
