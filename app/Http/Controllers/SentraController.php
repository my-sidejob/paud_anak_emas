<?php

namespace App\Http\Controllers;

use App\Sentra;
use Illuminate\Http\Request;

class SentraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sentra'] = Sentra::orderBy('nama_sentra', 'asc')->get();
        return view('admin.sentra.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['sentra'] = Sentra::getDefaultValues();
        return view('admin.sentra.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_sentra' => 'required',
        ]);
        Sentra::create($request->all());
        return redirect()->route('sentra.index')->with('success', 'Berhasil menambah data sentra');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sentra = Sentra::find($id);
        return view('admin.sentra.form', compact('sentra'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_sentra' => 'required'
        ]);
        
        $sentra = Sentra::find($id);

        $sentra->update($request->all());

        return redirect()->route('sentra.index')->with('success', 'Berhasil mengubah data sentra');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sentra = Sentra::find($id);

        $sentra->delete();

        return redirect()->route('sentra.index')->with('success', 'Berhasil menghapus data sentra');
    }
}
