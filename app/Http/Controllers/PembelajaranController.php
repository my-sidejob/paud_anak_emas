<?php

namespace App\Http\Controllers;

use App\Jadwal;
use App\Kelas;
use App\Nilai;
use App\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PembelajaranController extends Controller
{
    public function index(Request $request)
    {
        if(isset($request->siswa_id)){
            $data['siswa'] = Siswa::find($request->siswa_id);
            return view('orangtua.kelas_siswa', $data);
        } else {
            $data['siswa'] = Siswa::where('orangtua_id', Auth::guard('orangtua')->user()->id)->get();
            return view('orangtua.index_pembelajaran', $data);
        }
    }

    public function show($siswa_id, $kelas_id)
    {
        $data['siswa'] = Siswa::find($siswa_id);
        $data['kelas'] = Kelas::find($kelas_id);
        $data['menu'] = 1;
        $data['nilai'] = Nilai::getNilaiSiswa($siswa_id, $kelas_id);
        $data['jadwal'] = Jadwal::selectRaw("week(tanggal) AS week, kelas_id")->where('kelas_id', $kelas_id)->groupByRaw("week(tanggal), kelas_id")->orderBy('tanggal', 'asc')->get();
        return view('orangtua.show_pembelajaran', $data);
    }
}
