<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{
    protected $table = 'kegiatan';

    protected $fillable = [
        'nama_kegiatan',
        'keterangan',
        'sentra_id',        
    ];

    public static function getDefaultValues()
    {
        return (object) [
            'nama_kegiatan' => '',
            'keterangan' => '',
            'sentra_id' => ''
        ];
    }

    public function sentra()
    {
        return $this->belongsTo('App\Sentra');
    }
}
