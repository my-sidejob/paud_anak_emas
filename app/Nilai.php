<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nilai extends Model
{
    protected $table = 'nilai';

    protected $fillable = [
        'catatan_penilaian',
        'kegiatan_id',
        'siswa_id',
        'kelas_id',
        'user_id'
    ];

    public static function getDefaultValues()
    {
        return (object) [
            'catatan_penilaian' => '',
            'kegiatan_id' => '',
            'siswa_id' => '',
            'kelas_id' => '',
            'user_id' => ''
        ];
    }

    public function siswa()
    {
        return $this->belongsTo('App\Siswa');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function kegiatan()
    {
        return $this->belongsTo('App\Kegiatan');
    }

    public static function getNilaiSiswa($siswa_id, $kelas_id)
    {
        $q = Nilai::where('kelas_id', $kelas_id)->where('siswa_id', $siswa_id)->get();
        return $q;
    }
}
