<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisPembayaran extends Model
{
    protected $table = 'jenis_pembayaran';

    protected $fillable = [
        'nama_pembayaran',
        'biaya',        
        'keterangan',
        'status',
    ];

    public static function getDefaultValues()
    {
        return (object) [
            'nama_pembayaran' => '',
            'biaya' => '',
            'keterangan' => '',
            'status' => '',
        ];
    }

    public function getStatusBadge()
    {
        if($this->status == 0){
            return "<span class='badge badge-danger'>Non Aktif</span>";
        }
        if($this->status == 1){
            return "<span class='badge badge-primary'>Aktif</span>";
        }
    }
}
