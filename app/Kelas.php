<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = 'kelas';

    protected $fillable = [
        'nama_kelas',
        'tahun_ajar',        
        'keterangan',
        'tipe_kelas',
        'status'
    ];

    public static function getDefaultValues()
    {
        return (object) [
            'nama_kelas' => '',
            'tahun_ajar' => '',
            'keterangan' => '',
            'tipe_kelas' => '',
            'status' => ''
        ];
    }

    public function kelasSiswa()
    {
        return $this->belongsToMany('App\Siswa')->withTimestamps();
    }

    public function getStatusBadge()
    {
        if($this->status == 0){
            return "<span class='badge badge-danger'>Non Aktif</span>";
        }
        if($this->status == 1){
            return "<span class='badge badge-primary'>Aktif</span>";
        }
        
    }
}
