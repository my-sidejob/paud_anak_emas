<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswa';

    protected $fillable = [
        'nama_lengkap',
        'tempat_lahir',
        'tanggal_lahir',
        'jenis_kelamin',
        'status',
        'orangtua_id',
        'keterangan'
    ];

    protected $status_text = [
        '0' => 'Pending',
        '1' => 'Diterima',
        '2' => 'Aktif',
        '3' => 'Lulus A',
        '4' => 'Lulus B',
        '5' => 'Gagal',
        '6' => 'Ditolak',
    ];

    public static function getDefaultValues($type = null)
    {
        if($type == null){
            return (object) [
                'nama_lengkap' => '',
                'tempat_lahir' => '',
                'tanggal_lahir' => '',
                'jenis_kelamin' => '',
                'status' => '',
                'orangtua_id' => ''
            ];
        } else {
            return (object) [
                'nama_lengkap_anak' => '',
                'tempat_lahir' => '',
                'tanggal_lahir' => '',
                'jenis_kelamin' => '',
                'status' => '',
                'orangtua_id' => ''
            ];
        }
    }

    public function orangtua()
    {
        return $this->belongsTo('App\Orangtua');
    }

    public function getJenisKelamin()
    {
        if($this->jenis_kelamin == 'l'){
            return 'Laki - laki';
        } else {
            return 'Perempuan';
        }
    }

    public function getStatus()
    {
        return $this->status_text[$this->status];
    }

    public function getStatusBadge()
    {
        if($this->status == 0){
            return "<span class='badge badge-warning'>".$this->status_text[$this->status]."</span>";
        }
        if($this->status == 1){
            return "<span class='badge badge-info'>".$this->status_text[$this->status]."</span>";
        }
        if($this->status == 2){
            return "<span class='badge badge-primary'>".$this->status_text[$this->status]."</span>";
        }
        if($this->status == 3){
            return "<span class='badge badge-success'>".$this->status_text[$this->status]."</span>";
        }
        if($this->status == 4){
            return "<span class='badge badge-success'>".$this->status_text[$this->status]."</span>";
        }
        if($this->status == 5){
            return "<span class='badge badge-danger'>".$this->status_text[$this->status]."</span>";
        }
        if($this->status == 6){
            return "<span class='badge badge-danger'>".$this->status_text[$this->status]."</span>";
        }
    }

    public function getAge()
    {
        return Carbon::parse($this->attributes['tanggal_lahir'])->age;
    }

    public function kelasSiswa()
    {
        return $this->belongsToMany('App\Kelas')->withTimestamps();
    }

}
