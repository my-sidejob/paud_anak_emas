<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $table = 'pembayaran';

    protected $fillable = [
        'tanggal_pembayaran',
        'bukti_pembayaran',        
        'keterangan',
        'status',
        'siswa_id',
        'jenis_pembayaran_id',
    ];

    public function siswa()
    {
        return $this->belongsTo('App\Siswa');
    }

    public function jenisPembayaran()
    {
        return $this->belongsTo('App\JenisPembayaran');
    }

    public function getStatusBadge()
    {
        if($this->status == 0){
            return "<span class='badge badge-warning'>Menunggu Konfirmasi</span>";
        }
        if($this->status == 1){
            return "<span class='badge badge-success'>Berhasil</span>";
        }
        if($this->status == 2){
            return "<span class='badge badge-primary'>Gagal</span>";
        }
    }

}
