<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Orangtua extends Authenticatable
{
    protected $table = 'orangtua';

    protected $fillable = [
        'nama_lengkap',
        'username',
        'email',
        'password',
        'alamat',
        'no_telp'        
    ];

    public static function getDefaultValues()
    {
        return (object) [
            'nama_lengkap' => '',
            'username' => '',
            'email' => '',
            'password' => '',
            'alamat' => '',
            'no_telp' => '',
        ];
    }

    public function siswa()
    {
        return $this->hasMany('App\Siswa');
    }

    public function setPasswordAttribute($val)
    {
        return $this->attributes['password'] = bcrypt($val);
    }

}
