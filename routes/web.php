<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('register', 'RegisterSiswaController@index')->name('register.index');
Route::post('register/store', 'RegisterSiswaController@store')->name('register.store');
Route::post('orangtua/login', 'OrangtuaLoginController@login')->name('orangtua.login');

Route::prefix('ortu')->middleware(['auth:orangtua'])->group(function(){
    Route::get('dashboard', 'DashboardController@indexOrtu')->name('ortu.dashboard');
    Route::get('jenis-pembayaran', 'PembayaranController@jenisPembayaran')->name('ortu.jenis-pembayaran');
    Route::get('pembayaran', 'PembayaranController@indexOrtu')->name('ortu.pembayaran');
    Route::post('pembayaran/store', 'PembayaranController@store')->name('ortu.pembayaran.store');
    Route::get('riwayat-pembayaran', 'PembayaranController@riwayatPembayaran')->name('ortu.riwayat-pembayaran');
    Route::get('kritik-saran', 'KritikSaranController@create')->name('ortu.kritik-saran');
    Route::post('kritik-saran', 'KritikSaranController@store')->name('ortu.kritik-saran.store');
    Route::get('pembelajaran', 'PembelajaranController@index')->name('ortu.pembelajaran');
    Route::get('pembelajaran/{siswa_id}/{kelas_id}', 'PembelajaranController@show')->name('ortu.pembelajaran.show');

    Route::get('pendaftaran', 'SiswaController@daftarBaru')->name('ortu.daftar-baru');
    Route::post('pendaftaran/store', 'SiswaController@simpanDaftar')->name('ortu.daftar-baru.store');

    Route::get('ubah_password', 'UbahPasswordController@index')->name('ortu.ubah_password');
    Route::post('ubah_password', 'UbahPasswordController@change')->name('ortu.ubah_password.change');
});

Route::prefix('admin')->group(function(){
    Auth::routes();
});

Route::middleware(['auth'])->group(function(){
    Route::get('dashboard', 'DashboardController@index');
    Route::resource('sentra', 'SentraController');
    Route::resource('kelas', 'KelasController');
    Route::resource('kegiatan', 'KegiatanController');
    Route::resource('orangtua', 'OrangtuaController');
    Route::resource('user', 'UserController')->middleware('admin-only');
    Route::resource('siswa', 'SiswaController');
    Route::resource('kelas-siswa', 'KelasSiswaController');
    Route::resource('jadwal', 'JadwalController');
    Route::get('jadwal/detail/{kelas_id}/{tanggal}', 'JadwalController@jadwalHarian')->name('jadwal.detail_hari');
    Route::get('jadwal/kelas/{kelas_id}/', 'JadwalController@showJadwalKelas')->name('jadwal.kelas');
    Route::get('nilai/{siswa_id}/kelas/{kelas_id}', 'NilaiController@index')->name('nilai.index');
    Route::get('nilai/modal/{siswa_id}/{kelas_id}', 'NilaiController@create')->name('nilai.create');
    Route::post('nilai/store/{siswa_id}/{kelas_id}', 'NilaiController@store')->name('nilai.store');
    Route::get('nilai/{nilai_id}/edit', 'NilaiController@edit')->name('nilai.edit');
    Route::put('nilai/{nilai_id}/edit', 'NilaiController@update')->name('nilai.update');
    Route::delete('nilai/{id}/delete', 'NilaiController@delete')->name('nilai.destroy');
    Route::resource('jenis-pembayaran', 'JenisPembayaranController');
    Route::get('pembayaran', 'PembayaranController@index')->name('pembayaran.index');
    Route::get('pembayaran/{id}/change/{status}', 'PembayaranController@changeStatus')->name('pembayaran.change_status');
    Route::get('kritik-saran', 'KritikSaranController@index')->name('kritik-saran.index');
    Route::post('siswa-status/{siswa_id}', 'SiswaController@changeStatus')->name('siswa.changeStatus');

    Route::get('ubah_password', 'UbahPasswordController@index')->name('ubah_password');
    Route::post('ubah_password', 'UbahPasswordController@change')->name('ubah_password.change');
});

